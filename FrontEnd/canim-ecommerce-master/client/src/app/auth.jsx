/**
 * Title: Write a program using JavaScript on Auth
 * Author: Hasibul Islam
 * Portfolio: https://devhasibulislam.vercel.app
 * Linkedin: https://linkedin.com/in/devhasibulislam
 * GitHub: https://github.com/devhasibulislam
 * Facebook: https://facebook.com/devhasibulislam
 * Instagram: https://instagram.com/devhasibulislam
 * Twitter: https://twitter.com/devhasibulislam
 * Pinterest: https://pinterest.com/devhasibulislam
 * WhatsApp: https://wa.me/8801906315901
 * Telegram: devhasibulislam
 * Date: 14, November 2023
 */

"use client";

import { useEffect, useMemo } from "react";
import { useDispatch } from "react-redux";
import { usePersistLoginQuery } from "@/services/auth/authApi";
import { addUser } from "@/features/auth/authSlice";
import { addUserL } from "@/features/auth/authSlice";
import { toast } from "react-hot-toast";

const Auth = ({ children }) => {
  const dispatch = useDispatch();
  const { data: userData, error: userError } = usePersistLoginQuery();
  const user = useMemo(() => userData || {}, userData);

  useEffect(() => {
    if (userData && !userError) {
      toast.success(userData?.description, { id: "auth" });
      dispatch(addUser(JSON.stringify(user)));
      localStorage.setItem("user", JSON.stringify(userData));
    }
    if (userError) {
      dispatch(addUser(JSON.stringify({})));
    }

    if (userError?.data) {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("user");
      // toast.error(userError?.data?.description, { id: "auth" });
      dispatch(addUser(JSON.stringify({})));
    }
  }, [userData, userError, dispatch, user]);

  return <>{children}</>;
};

export default Auth;
