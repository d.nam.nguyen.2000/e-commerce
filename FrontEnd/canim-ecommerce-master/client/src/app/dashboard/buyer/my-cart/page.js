/**
 * Title: Write a program using JavaScript on Page
 * Author: Hasibul Islam
 * Portfolio: https://devhasibulislam.vercel.app
 * Linkedin: https://linkedin.com/in/devhasibulislam
 * GitHub: https://github.com/devhasibulislam
 * Facebook: https://facebook.com/devhasibulislam
 * Instagram: https://instagram.com/devhasibulislam
 * Twitter: https://twitter.com/devhasibulislam
 * Pinterest: https://pinterest.com/devhasibulislam
 * WhatsApp: https://wa.me/8801906315901
 * Telegram: devhasibulislam
 * Date: 14, January 2024
 */

"use client";

import Inform from "@/components/icons/Inform";
import Trash from "@/components/icons/Trash";
import Dashboard from "@/components/shared/layouts/Dashboard";
import { useDeleteFromCartMutation, useGetFromCartQuery } from "@/services/cart/cartApi";
import Image from "next/image";
import React, {useEffect, useState} from "react";
import { toast } from "react-hot-toast";
import { useSelector } from "react-redux";

const Page = () => {
  // const user = useSelector((state) => state.auth.user);

  const user = useSelector((state) => {
    const userJson = state?.auth?.user;
    if (typeof userJson === 'string' && userJson.trim() !== '') {
      return userJson !== "{}" ? JSON.parse(userJson) : null;
    } else {
      return null;
    }
  });

  const [cart, setCart] = useState([]);
  // const { isLoading1, data1, error1 } = useGetFromCartQuery();
  //
  // useEffect(() => {
  //   if (isLoading1) {
  //     toast.loading("Loading item from cart...", { id: "loadFromCart" });
  //   }
  //
  //   if (data1) {
  //     toast.success(data1?.description, { id: "loadFromCart" });
  //     console.log("cart: ", cart);
  //     setCart(data1);
  //   }
  //
  //   if (error1?.data) {
  //     toast.error(error1?.data?.description, { id: "loadFromCart" });
  //   }
  // }, [isLoading1, data1, error1]);

  const [removeFromCart, { isLoading, data, error }] =
    useDeleteFromCartMutation();

  useEffect(() => {
    // setCart(user?.cart)
    if (isLoading) {
      toast.loading("Removing item from cart...", { id: "removeFromCart" });
    }

    if (data) {
      toast.success("Success", { id: "removeFromCart" });
    }

    if (error?.data) {
      toast.error(error?.data?.message, { id: "removeFromCart" });
    }
  }, [isLoading, data, error]);

  return (
    <Dashboard>
      {user?.cart?.length === 0 ? (
        <p className="text-sm flex flex-row gap-x-1 items-center justify-center">
          <Inform /> Không có sản phẩm nào trong giỏ hàng?
        </p>
      ) : (
        <section className="w-full h-full">
          <div className="overflow-x-auto w-full">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-slate-100">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Ảnh sản phẩm
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Tên sản phẩm
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Số lượng
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Giá
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Gallery
                  </th>
                  {/*<th*/}
                  {/*  scope="col"*/}
                  {/*  className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"*/}
                  {/*>*/}
                  {/*  Sizes*/}
                  {/*</th>*/}
                  {/*<th*/}
                  {/*  scope="col"*/}
                  {/*  className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"*/}
                  {/*>*/}
                  {/*  Colors*/}
                  {/*</th>*/}
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Danh mục
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Thương hiệu
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Cửa hàng
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-right text-xs font-bold text-gray-500 uppercase whitespace-nowrap"
                  >
                    Hành Động
                  </th>
                </tr>
              </thead>
              <tbody>
                {user?.cart?.map(({ product, quantity, id }) => (
                  <tr
                    key={product?.id}
                    className="odd:bg-white even:bg-gray-100 hover:odd:bg-gray-100"
                  >
                    <td className="px-6 py-4">
                      <img
                        src={product?.thumbnailUrl}
                        // alt={product?.thumbnail?.public_id}
                        height={30}
                        width={30}
                        className="h-[30px] w-[30px] rounded-secondary border border-green-500/50 object-cover"
                      />
                    </td>
                    <td className="px-6 py-4">
                      <span className="whitespace-nowrap w-60 overflow-x-auto block scrollbar-hide text-sm">
                        {product?.title}
                      </span>
                    </td>
                    <td className="px-6 py-4">
                      <span className="whitespace-nowrap scrollbar-hide text-sm">
                        {quantity}
                      </span>
                    </td>
                    <td className="px-6 py-4">
                      <span className="whitespace-nowrap scrollbar-hide text-sm">
                        {product?.price * quantity}
                      </span>
                    </td>
                    <td className="px-6 py-4">
                      <div className="flex -space-x-4">
                        {product?.gallery?.map((thumbnail) => (
                          <img
                             key={thumbnail?.id}
src={thumbnail}
                            height={30}
                            width={30}
                            className="h-[30px] w-[30px] rounded-secondary border border-green-500/50 object-cover"
                          />
                        ))}
                      </div>
                    </td>
                    {/*<td className="px-6 py-4">*/}
                    {/*  <span className="flex flex-row gap-x-2 scrollbar-hide text-sm">*/}
                    {/*    {product?.variations?.sizes?.map((size) => (*/}
                    {/*      <span key={size} className="border px-1 py-0.5">*/}
                    {/*        {size.toUpperCase()}*/}
                    {/*      </span>*/}
                    {/*    ))}*/}
                    {/*  </span>*/}
                    {/*</td>*/}
                    {/*<td className="px-6 py-4">*/}
                    {/*  <span className="flex flex-row gap-x-2 scrollbar-hide text-sm">*/}
                    {/*    {product?.variations?.colors?.map((color) => (*/}
                    {/*      <span*/}
                    {/*        key={color}*/}
                    {/*        style={{*/}
                    {/*          backgroundColor: `#${color}`,*/}
                    {/*          height: "20px",*/}
                    {/*          width: "20px",*/}
                    {/*        }}*/}
                    {/*      />*/}
                    {/*    ))}*/}
                    {/*  </span>*/}
                    {/*</td>*/}
                    <td className="px-6 py-4">
                      <span className="whitespace-nowrap scrollbar-hide text-sm">
                        {product?.category?.title}
                      </span>
                    </td>
                    <td className="px-6 py-4">
                      <span className="whitespace-nowrap scrollbar-hide text-sm">
                        {product?.brand?.title}
                      </span>
                    </td>
                    <td className="px-6 py-4">
                      <span className="whitespace-nowrap scrollbar-hide text-sm">
                        {product?.store?.title}
                      </span>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                      <button
                        type="submit"
                        className="bg-red-50 border border-red-900 p-0.5 rounded-secondary text-red-900"
                        onClick={() => removeFromCart(id)}
                      >
                        <Trash />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </section>
      )}
    </Dashboard>
  );
};

export default Page;
