/**
 * Title: Write a program using JavaScript on Page
 * Author: Hasibul Islam
 * Portfolio: https://devhasibulislam.vercel.app
 * Linkedin: https://linkedin.com/in/devhasibulislam
 * GitHub: https://github.com/devhasibulislam
 * Facebook: https://facebook.com/devhasibulislam
 * Instagram: https://instagram.com/devhasibulislam
 * Twitter: https://twitter.com/devhasibulislam
 * Pinterest: https://pinterest.com/devhasibulislam
 * WhatsApp: https://wa.me/8801906315901
 * Telegram: devhasibulislam
 * Date: 15, January 2024
 */

"use client";

import Inform from "@/components/icons/Inform";
import Minus from "@/components/icons/Minus";
import Plus from "@/components/icons/Plus";
import Trash from "@/components/icons/Trash";
import Modal from "@/components/shared/Modal";
import Dashboard from "@/components/shared/layouts/Dashboard";
import {
  useAddBrandMutation,
  useDeleteBrandMutation,
  useUpdateBrandMutation,
  useGetBrandsQuery,
} from "@/services/brand/brandApi";
import Image from "next/image";
import React, {useEffect, useMemo, useState} from "react";
import { toast } from "react-hot-toast";
import {useDispatch, useSelector} from "react-redux";
import {formDataToJson} from "../../../../services/convert";
import {useImageUpload} from "../../../../services/useImageUpload";

const Page = () => {
  // const userInfo = useSelector((state) => state.auth.user);

  // const userInfo = useSelector((state) => {
  //   const userJson = state?.auth?.user;
  //   if (typeof userJson === 'string' && userJson.trim() !== '') {
  //     return userJson !== "{}" ? JSON.parse(userJson) : null;
  //   } else {
  //     return null;
  //   }
  // });

  const { data: brandData, error: brandError } = useGetBrandsQuery();

  return (
    <Dashboard><AddBrand /></Dashboard>
  );
};

function AddBrand() {
  const [logo, setLogo] = useState(null);
  const [logoPreview, setLogoPreview] = useState(null);
  const [keynotes, setKeynotes] = useState([""]);
  const [tags, setTags] = useState([""]);
  const [addBrand, { isLoading, data, error }] = useAddBrandMutation();

  useEffect(() => {
    if (isLoading) {
      toast.loading("Adding brand...", { id: "addBrand" });
    }

    if (data) {
      toast.success("Success", { id: "addBrand" });
    }

    if (error?.data) {
      toast.error(error?.data?.message, { id: "addBrand" });
    }
  }, [isLoading, data, error]);

  /* for logo preview */
  // const handleLogoPreview = (e) => {
  //     setLogo(e.target.files[0]);
  //
  //     const file = e.target.files[0];
  //
  //     if (file) {
  //         const reader = new FileReader();
  //         reader.onloadend = () => {
  //             setLogoPreview(reader.result);
  //         };
  //         reader.readAsDataURL(file);
  //     }
  // };

  const { uploadImgMutation } = useImageUpload();
  const handleAvatarChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        setLogoPreview(reader.result);
      };

      const formData = new FormData();
      formData.append("file", file);

      reader.readAsDataURL(file);

      uploadImgMutation(formData)
          .then(response => {

            setLogo(response.error.data);
          })
          .catch(error => {
            console.error('Failed to upload image:', error);
          });
    }
  };

  /* for keynotes */
  const handleAddKeynote = () => {
    setKeynotes([...keynotes, ""]);
  };

  const handleRemoveKeynote = (index) => {
    const updatedKeynotes = [...keynotes];
    updatedKeynotes.splice(index, 1);
    setKeynotes(updatedKeynotes);
  };

  const handleKeynoteChange = (index, value) => {
    const updatedKeynotes = [...keynotes];
    updatedKeynotes[index] = value;
    setKeynotes(updatedKeynotes);
  };

  /* for tags */
  const handleAddTag = () => {
    setTags([...tags, ""]);
  };

  const handleRemoveTag = (index) => {
    const updatedTags = [...tags];
    updatedTags.splice(index, 1);
    setTags(updatedTags);
  };

  const handleTagChange = (index, value) => {
    const updatedTags = [...tags];
    updatedTags[index] = value;
    setTags(updatedTags);
  };

  function handleAddBrand(e) {
    e.preventDefault();

    const formData = new FormData();

    formData.append("logoUrl", logo);

    formData.append("keynotes", JSON.stringify(keynotes));
    formData.append("tags", JSON.stringify(tags));

    formData.append("title", e.target.title.value);
    formData.append("description", e.target.description.value);

    addBrand(formDataToJson(formData));

    e.target.reset();
    setLogoPreview(null);
    setKeynotes([""]);
    setTags([""]);
  }

  return (
      <form
          action=""
          className="w-full flex flex-col gap-y-4"
          onSubmit={handleAddBrand}
      >
        {/* logo */}
        <div className="w-fit flex flex-col gap-y-4 p-4 border rounded">
          {logoPreview && (
              <img
                  src={logoPreview}
                  alt={"logo"}
                  width={96}
                  height={96}
                  className="w-full h-24 object-cover rounded"
              />
          )}

          <label htmlFor="logo" className="w-full flex flex-col gap-y-1 relative">
            <span className="text-sm cursor-pointer">Chọn ảnh*</span>
            <input
                type="file"
                name="logo"
                id="logo"
                className="w-full h-full opacity-0 absolute top-0 left-0 cursor-pointer z-50"
                accept=".jpg, .jpeg, .png"
                multiple={false}
                onChange={handleAvatarChange}
                required
            />
          </label>
        </div>

        {/* title & description */}
        <div className="w-full flex flex-col gap-y-4 p-4 border rounded">
          {/* title */}
          <label htmlFor="title" className="w-full flex flex-col gap-y-1">
            <span className="text-sm">Tên*</span>
            <input type="text" name="title" id="title" maxlength="100" required />
          </label>

          {/* description */}
          <label htmlFor="email" className="w-full flex flex-col gap-y-1">
            <span className="text-sm">Mô tả*</span>
            <textarea name="description" id="description" rows="4" maxlength="500" required />
          </label>
        </div>

        {/* keynotes */}
        <div className="w-full flex flex-col gap-y-4 p-4 border rounded">
          <label htmlFor="keynotes" className="w-full flex flex-col gap-y-4">
            <p className="text-sm flex flex-row justify-between items-center">
              Keynotes*
              <button
                  type="button"
                  className="p-0.5 border rounded-secondary bg-green-500 text-white"
                  onClick={handleAddKeynote}
              >
                <Plus />
              </button>
            </p>

            {keynotes.map((keynote, index) => (
                <p key={index} className="flex flex-row gap-x-2 items-center">
                  <input
                      type="text"
                      name="keynotes"
                      placeholder="Từ khóa"
                      className="flex-1"
                      value={keynote}
                      onChange={(event) =>
                          handleKeynoteChange(index, event.target.value)
                      }
                      required
                  />
                  {index !== 0 && (
                      <button
                          type="button"
                          className="p-0.5 border rounded-secondary bg-red-500 text-white"
                          onClick={() => handleRemoveKeynote(index)}
                      >
                        <Minus />
                      </button>
                  )}
                </p>
            ))}
          </label>
        </div>

        {/* tags */}
        <div className="w-full flex flex-col gap-y-4 p-4 border rounded">
          <label htmlFor="tags" className="w-full flex flex-col gap-y-4">
            <p className="text-sm flex flex-row justify-between items-center">
              Tags*
              <button
                  type="button"
                  className="p-0.5 border rounded-secondary bg-green-500 text-white"
                  onClick={handleAddTag}
              >
                <Plus />
              </button>
            </p>

            {tags.map((tag, index) => (
                <p key={index} className="flex flex-row gap-x-2 items-center">
                  <input
                      type="text"
                      name="tags"
                      placeholder="Thẻ"
                      className="flex-1"
                      value={tag}
                      onChange={(event) => handleTagChange(index, event.target.value)}
                      required
                  />
                  {index !== 0 && (
                      <button
                          type="button"
                          className="p-0.5 border rounded-secondary bg-red-500 text-white"
                          onClick={() => handleRemoveTag(index)}
                      >
                        <Minus />
                      </button>
                  )}
                </p>
            ))}
          </label>
        </div>

        {/* submit button */}
        <input
            type="submit"
            value="Tạo thương hiệu"
            className="py-2 border border-black rounded bg-black hover:bg-black/90 text-white transition-colors drop-shadow cursor-pointer"
        />
      </form>
  );
}

export default Page;
