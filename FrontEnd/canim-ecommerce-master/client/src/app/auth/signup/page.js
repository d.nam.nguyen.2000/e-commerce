/**
 * Title: Write a program using JavaScript on Signup
 * Author: Hasibul Islam
 * Portfolio: https://devhasibulislam.vercel.app
 * Linkedin: https://linkedin.com/in/devhasibulislam
 * GitHub: https://github.com/devhasibulislam
 * Facebook: https://facebook.com/devhasibulislam
 * Instagram: https://instagram.com/devhasibulislam
 * Twitter: https://twitter.com/devhasibulislam
 * Pinterest: https://pinterest.com/devhasibulislam
 * WhatsApp: https://wa.me/8801906315901
 * Telegram: devhasibulislam
 * Date: 08, November 2023
 */

"use client";

import Trash from "@/components/icons/Trash";
import Upload from "@/components/icons/Upload";
import Spinner from "@/components/shared/Spinner";
import { useSignUpMutation } from "@/services/auth/authApi";
import { useImageUpload } from '../../../services/useImageUpload'
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import { toast } from "react-hot-toast";
import {formDataToJson} from "../../../services/convert";

const Signup = () => {
  const router = useRouter();
  const [avatarPreview, setAvatarPreview] = useState(null);
  const [avatar, setAvatar] = useState(null);

  const [signup, { isLoading, data, error }] = useSignUpMutation();

  useEffect(() => {
    if (isLoading) {
      toast.loading("Signing up...", { id: "signup" });
    }

    if (data) {
      toast.success("Success", { id: "signup" });
      
      // open new tab
      setTimeout(() => {
        window.open("/auth/signin", "_self");
      }, 1000);
    }
    if (error?.data) {
      toast.error(error?.data?.message, { id: "signup" });
    }
  }, [isLoading, data, error, router]);

  // const handleAvatarChange = (e) => {
  //   const file = e.target.files[0];
  //   setAvatar(file);
  //
  //   if (!avatarPreview) {
  //     if (file) {
  //       const reader = new FileReader();
  //       reader.onloadend = () => {
  //         setAvatarPreview(reader.result);
  //       };
  //
  //       reader.readAsDataURL(file);
  //     }
  //   }
  // };
  const { uploadImgMutation } = useImageUpload();
  const handleAvatarChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        setAvatarPreview(reader.result);
      };

      const formData = new FormData();
      formData.append("file", file);

      reader.readAsDataURL(file);

      uploadImgMutation(formData)
          .then(response => {

            setAvatar(response.error.data);
          })
          .catch(error => {
            console.error('Failed to upload image:', error);
          });
    }
  };

  const handleSignup = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("avatarUrl", avatar);

    formData.append("name", e.target.name.value);
    formData.append("email", e.target.email.value);

    // Phone number validation regex
    const phoneRegex = /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/;

    if (!e.target.phone.value.match(phoneRegex)) {
      alert(
        "Phone number not correct!"
      );
      return;
    }

    formData.append("phone", e.target.phone.value);
    formData.append("password", e.target.password.value);
    formData.append("role", "buyer");

    signup(formDataToJson(formData));

    e.target.reset();
    setAvatarPreview(null);
  };

  return (
    <section className="min-w-full min-h-screen flex justify-center items-center p-4">
      <div className="max-w-md w-full flex flex-col gap-y-4 border p-8 rounded-primary">
        <div className="flex flex-row items-center gap-x-2">
          <hr className="w-full" />
          <img
            src="/logo.png"
            alt="logo"
            width={141}
            height={40}
            className="max-w-full cursor-pointer"
            onClick={() => router.push("/")}
          />
          <hr className="w-full" />
        </div>
        <form
          action=""
          className="w-full flex flex-col gap-y-4"
          onSubmit={handleSignup}
        >
          <label
            htmlFor="avatar"
            className="flex flex-col gap-y-1 w-fit mx-auto items-center"
          >
            <div
              className={
                "h-[100px] w-[100px] rounded transition-colors flex flex-row justify-center items-center relative" +
                " " +
                (avatarPreview
                  ? ""
                  : "border-2 border-dashed hover:border-black")
              }
            >
              {avatarPreview ? (
                <div className="relative">
                  <img
                    src={avatarPreview}
                    alt="avatar"
                    height={100}
                    width={100}
                    className="rounded h-[100px] w-[100px] object-cover"
                  />
                  <button
                    className="absolute bottom-0 -right-10 p-1 rounded bg-red-500 text-white shadow-2xl"
                    onClick={() => setAvatarPreview(null)}
                  >
                    <Trash />
                  </button>
                </div>
              ) : (
                <>
                  <span className="text-xs flex flex-col justify-center items-center gap-y-2 text-center">
                    <Upload />
                    Add Avatar <br /> 300x300
                  </span>

                  <input
                    type="file"
                    name="avatar"
                    id="avatar"
                    title="Dimension: 300x300"
                    accept=".jpg, .jpeg, .png"
                    className="absolute top-0 left-0 w-full h-full opacity-0 cursor-pointer"
                    onChange={handleAvatarChange}
                  />
                </>
              )}
            </div>
          </label>
          <label htmlFor="name" className="flex flex-col gap-y-1">
            <span className="text-sm">Enter Your Name*</span>
            <input
              type="text"
              name="name"
              id="name"
              placeholder="i.e. John Doe"
              className=""
              required
            />
          </label>
          <label htmlFor="email" className="flex flex-col gap-y-1">
            <span className="text-sm">Enter Your Email*</span>
            <input
              type="email"
              name="email"
              id="email"
              placeholder="i.e. example@gmail.com"
              className=""
              required
            />
          </label>
          <label htmlFor="password" className="flex flex-col gap-y-1">
            <span className="text-sm">Enter Your Password*</span>
            <input
              type="password"
              name="password"
              id="password"
              placeholder="password"
              className=""
              required
            />
          </label>
          <label htmlFor="phone" className="flex flex-col gap-y-1">
            <span className="text-sm">Enter Your Phone Number*</span>
            <input
              type="tel"
              name="phone"
              id="phone"
              placeholder="phone number"
              className=""
              required
            />
          </label>
          <button
            type="submit"
            disabled={isLoading}
            className="py-2 border border-black rounded-secondary bg-black hover:bg-black/90 text-white transition-colors drop-shadow disabled:bg-gray-200 disabled:border-gray-200 disabled:text-black/50 disabled:cursor-not-allowed flex flex-row justify-center items-center text-sm"
          >
            {isLoading ? <Spinner /> : "Đăng Ký"}
          </button>
        </form>
        <div className="flex flex-row justify-center items-center gap-x-2 text-xs">
          <Link href="/auth/signin" className="">
            Đăng nhập
          </Link>
          <span className="h-4 border-l" />
          <Link href="/auth/forgot-password" className="">
            Quên mật khẩu
          </Link>
        </div>
      </div>
    </section>
  );
};

export default Signup;
