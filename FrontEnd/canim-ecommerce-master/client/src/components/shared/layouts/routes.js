const routes = [
  {
    name: "Sản ph",
    paths: [
      {
        name: "Thêm Sản Phẩm",
        path: "/dashboard/add-product",
      },
      {
        name: "Sản Phẩm",
        path: "/dashboard/list-products",
      },
    ],
  },
  {
    name: "Danh mục",
    paths: [
      {
        name: "Thêm danh mục",
        path: "/dashboard/add-category",
      },
      {
        name: "Danh Mục",
        path: "/dashboard/list-categories",
      },
    ],
  },
  {
    name: "Cửa hàng",
    paths: [
      {
        name: "Thêm cửa hàng",
        path: "/dashboard/add-store",
      },
      {
        name: "Cửa Hàng",
        path: "/dashboard/list-stores",
      },
    ],
  },
  {
    name: "Thương hiệu",
    paths: [
      {
        name: "Thêm thương hiệu",
        path: "/dashboard/add-brand",
      },
      {
        name: "Thương Hiệu",
        path: "/dashboard/list-brands",
      },
    ],
  },
  {
    name: "User",
    paths: [
      {
        name: "Add User",
        path: "/dashboard/add-user",
      },
      {
        name: "List Users",
        path: "/dashboard/list-users",
      },
    ],
  },
];

export default routes;
