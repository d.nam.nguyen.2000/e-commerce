// UserModel.js
class User {
    constructor(id, name, email, avatarUrl, phone, role, status, address, createdAt, updatedAt) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.avatarUrl = avatarUrl;
        this.phone = phone;
        this.role = role;
        this.status = status;
        this.address = address;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        // Bổ sung các thuộc tính khác của DTO nếu cần
    }
}

export default User;
