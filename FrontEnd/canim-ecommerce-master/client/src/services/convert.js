// formDataUtils.js
export function formDataToJson(formData) {
    const jsonData = {};
    formData.forEach((value, key) => {
        if (jsonData[key] !== undefined) {
            if (!Array.isArray(jsonData[key])) {
                jsonData[key] = [jsonData[key]];
            }
            jsonData[key].push(value);
        } else {
            jsonData[key] = value;
        }
    });
    return jsonData;
}
