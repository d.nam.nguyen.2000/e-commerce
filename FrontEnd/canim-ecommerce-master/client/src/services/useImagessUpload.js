import { useUploadImgesMutation } from "@/services/store/storeApi";

export const useImagessUpload = () => {
    const [uploadImgesMutation, { isLoading, data, error }] = useUploadImgesMutation();

    return { uploadImgesMutation, isLoading, data, error };
};
