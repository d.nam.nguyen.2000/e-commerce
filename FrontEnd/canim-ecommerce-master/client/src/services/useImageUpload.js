import { useUploadImgMutation } from "@/services/store/storeApi";

export const useImageUpload = () => {
    const [uploadImgMutation, { isLoading, data, error }] = useUploadImgMutation();

    return { uploadImgMutation, isLoading, data, error };
};
