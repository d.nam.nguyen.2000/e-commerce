package com.ecomerce.product.domain.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.beans.FeatureDescriptor;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Tuple;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class FnCommon {
  private static ModelMapper modelMapper;

  public static ModelMapper getModelMapper() {
    if (modelMapper == null) {
      modelMapper = new ModelMapper();
    }

    return modelMapper;
  }

  public static void coppyProperties(Object target, Object source) {
    BeanUtils.copyProperties(source, target);
  }

  public static void coppyNonNullProperties(Object target, Object source) {
    BeanUtils.copyProperties(source, target, getNullPropertyNames(source));
  }

  public static <T> T copyNonNullProperties(Class<T> clazz, Object source) {
    try {
      Constructor<?> targetIntance = clazz.getDeclaredConstructor();
      targetIntance.setAccessible(true);

      T target = (T) targetIntance.newInstance();
      BeanUtils.copyProperties(source, target, getNullPropertyNames(source));

      return target;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String[] getNullPropertyNames(Object source) {
    BeanWrapper wrappedSource = new BeanWrapperImpl(source);
    return Stream.of(wrappedSource.getPropertyDescriptors())
        .map(FeatureDescriptor::getName)
        .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
        .toArray(String[]::new);
  }

  public static void coppyNonNullPropertiesIgnore(Object target, Object source, String... field) {
    String[] f = getNullPropertyNames(source);
    List<String> s = Arrays.asList(f.clone());
    List<String> a = Arrays.asList(field);
    List<String> newList = Stream.concat(s.stream(), a.stream()).collect(Collectors.toList());
    BeanUtils.copyProperties(source, target, newList.stream().toArray(String[]::new));
  }

  public static void coppyPropertiesIgnore(Object target, Object source, String... field) {
    List<String> a = Arrays.asList(field);
    BeanUtils.copyProperties(source, target, a.stream().toArray(String[]::new));
  }

  public static boolean isEmail(String email) {
    Pattern EMAIL_REGEX =
        Pattern.compile(
            "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
            Pattern.CASE_INSENSITIVE);
    return EMAIL_REGEX.matcher(email).matches();
  }

  public static boolean isPhoneNumber(String phone) {
    Pattern PHONE_REGX =
        Pattern.compile("(09|03|08|07|05|\\+84[9|3|8|7])+([0-9]{8})", Pattern.CASE_INSENSITIVE);
    return PHONE_REGX.matcher(phone).matches();
  }

  public static List<?> convertToEntity(List<Tuple> input, Class<?> dtoClass) {
    List<Object> arrayList = new ArrayList();
    input.stream()
        .forEach(
            (tuple) -> {
              Map<String, Object> temp = new HashMap();
              tuple.getElements().stream()
                  .forEach(
                      (tupleElement) -> {
                        Object value = tuple.get(tupleElement.getAlias());
                        temp.put(tupleElement.getAlias().toLowerCase(), value);
                      });
              ObjectMapper map = new ObjectMapper();
              map.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
              map.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

              try {
                String mapToString = map.writeValueAsString(temp);
                arrayList.add(map.readValue(mapToString, dtoClass));
              } catch (JsonProcessingException var6) {
                throw new RuntimeException(var6.getMessage());
              }
            });
    return arrayList;
  }
}
