package com.ecomerce.product.app.dtos;

import lombok.Data;

@Data
public class BrandDto {
    private String title;
    private String description;
    private String logoUrl;
    private String keynotes;
    private String tags;
}
