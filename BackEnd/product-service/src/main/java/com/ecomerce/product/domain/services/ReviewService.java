package com.ecomerce.product.domain.services;

import com.ecomerce.product.app.dtos.ReviewDto;
import com.ecomerce.product.domain.entities.Review;
import com.ecomerce.product.domain.exception.AppException;
import com.ecomerce.product.domain.repositories.ReviewRepository;
import com.ecomerce.product.domain.utils.FnCommon;
import com.ecomerce.product.domain.utils.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewService extends BaseService{
    @Autowired
    private ReviewRepository reviewRepository;

    public Review addReview(Long userId, ReviewDto ReviewDto) {
        Review Review = FnCommon.copyNonNullProperties(Review.class, ReviewDto);
        Review.setReviewerId(userId);

        return reviewRepository.save(Review);
    }

    public List<Review> getReviews(Long userId, Long shopId, String role) {
        if (role == null || role.equals("admin")) {
            return reviewRepository.findAll();
        } else {
            return reviewRepository.findAllByReviewerId(userId);
        }

    }

    public Review getReviewById(Long id) {
        return reviewRepository.findById(id)
                .orElseThrow(() -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("Review_not_found")));
    }

    public Review updateReview(Long id, Long userId, ReviewDto dto) {
        Review Review = getReviewById(id);
        FnCommon.coppyNonNullProperties(Review, dto);
        return reviewRepository.save(Review);
    }

    public void deleteReview(Long id) {
        Review Review = getReviewById(id);
        reviewRepository.delete(Review);
    }
}
