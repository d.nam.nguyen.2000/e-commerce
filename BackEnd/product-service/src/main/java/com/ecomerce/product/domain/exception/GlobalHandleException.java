package com.ecomerce.product.domain.exception;

import com.ecomerce.product.domain.utils.Constant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
@Log4j2
public class GlobalHandleException extends ResponseEntityExceptionHandler {

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ExceptionResponse> globalExceptionHandler(
    Exception ex, HttpServletRequest request) {
    if (ex instanceof ExceptionOm) {
      ExceptionOm exceptionOm = (ExceptionOm) ex;
      ExceptionResponse exceptionResponse =
        new ExceptionResponse(
          exceptionOm.status,
          new Date(),
          exceptionOm.getMessage(),
          exceptionOm.getMessage(),
          request.getServletPath());
      return new ResponseEntity<>(exceptionResponse, exceptionOm.status);
    }
    ExceptionResponse exceptionResponse =
      new ExceptionResponse(
        INTERNAL_SERVER_ERROR,
        new Date(),
        "Đã có lỗi xảy ra.",
        ex.getMessage(),
        request.getServletPath());
    return new ResponseEntity<>(exceptionResponse, INTERNAL_SERVER_ERROR);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
    MethodArgumentNotValidException ex,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request) {
    ValidDetails validDetails = new ValidDetails();
    Map<String, String> message = new HashMap<>();
    if (ex instanceof MethodArgumentNotValidException) {
      List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
      for (FieldError fieldError : fieldErrors) {
        message.put(fieldError.getField(), fieldError.getDefaultMessage());
      }
      validDetails.setMessage(message);
    } else {
      message.put("default", ex.getLocalizedMessage());
      validDetails.setMessage(message);
    }
    validDetails.setStatus(HttpStatus.BAD_REQUEST.value());
    validDetails.setTimestamp(new Date());
    validDetails.setError("Not valid exception");
    validDetails.setPath(((ServletWebRequest) request).getRequest().getServletPath());
    return new ResponseEntity(validDetails, status);
  }

  @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
  public ResponseEntity<ExceptionResponse> notFoundException(
          ChangeSetPersister.NotFoundException ex, WebRequest request) {
    ExceptionResponse errorDetails =
      new ExceptionResponse(
        HttpStatus.NOT_FOUND,
        new Date(),
        ex.getMessage(),
        ex.getMessage(),
        ((ServletWebRequest) request).getRequest().getServletPath());
    return new ResponseEntity<>(errorDetails, HttpStatus.OK);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    ExceptionResponse exceptionResponse =
      new ExceptionResponse(
        BAD_REQUEST,
        new Date(),
        "Malformed JSON request",
        ex.getLocalizedMessage(),
        ((ServletWebRequest) request).getRequest().getServletPath());
    return new ResponseEntity<>(exceptionResponse,BAD_REQUEST);
  }

  @ExceptionHandler(MissingRequestHeaderException.class)
  public ResponseEntity<ExceptionResponse> handleException(MissingRequestHeaderException ex) {
//    if(ex.getMessage().contains(Constant.headerPrefix)){
//      ExceptionResponse errorDetails =
//        new ExceptionResponse(
//          HttpStatus.UNAUTHORIZED,
//          new Date(),
//          "unauthorized",
//          "unauthorized",
//          null);
//      ex.printStackTrace();
//      return new ResponseEntity<>(errorDetails, HttpStatus.UNAUTHORIZED);
//    }
    ExceptionResponse errorDetails =
      new ExceptionResponse(
        HttpStatus.BAD_REQUEST,
        new Date(),
        ex.getMessage(),
        ex.getMessage(),
        null);
    ex.printStackTrace();

    return new ResponseEntity<>(errorDetails, BAD_REQUEST);
  }
}
