package com.ecomerce.product.domain.repositories;

import com.ecomerce.product.domain.entities.Brand;
import com.ecomerce.product.domain.entities.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
    List<Review> findAllByReviewerId(Long userId);
}
