package com.ecomerce.product.domain.entities.enums;

public enum Actor {
  ADMIN,
  SHOP,
  USER
}
