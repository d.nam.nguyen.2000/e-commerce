package com.ecomerce.product.domain.entities.view;

import com.ecomerce.product.domain.entities.enums.StoreState;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@ToString
@Table(name = "stores")
@TypeDef(name = "pg-string-array", typeClass = StringArrayType.class)
@TypeDef(name = "pg-enum", typeClass = PostgreSQLEnumType.class)

public class Shop {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "title", length = 100, nullable = false)
  private String title;

  @Column(name = "description")
  private String description;

  @Column(name = "thumbnail")
  private String thumbnail;

  @Column(name = "owner_id")
  private Long ownerId;

//    @ElementCollection
//    @Column(name = "products_id", columnDefinition = "INTEGER[]")
//    private List<Integer> productsId;

  @Column(name = "status", length = 10, columnDefinition = "VARCHAR(10) DEFAULT 'active'")
  private String status;

//    @ElementCollection
//    @Column(name = "keynotes", columnDefinition = "TEXT[]")
//    private List<String> keynotes;

//    @ElementCollection
//    @Column(name = "tags", columnDefinition = "TEXT[]")
//    private List<String> tags;

  @CreationTimestamp
  @Column(name = "created_at")
  private Timestamp createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at")
  private Timestamp updatedAt;
}
