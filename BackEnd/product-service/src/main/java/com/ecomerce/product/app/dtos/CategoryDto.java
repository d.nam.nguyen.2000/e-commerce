package com.ecomerce.product.app.dtos;

import com.ecomerce.product.domain.entities.enums.CategoryState;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CategoryDto {
  private String title;
  private String description;
  private String thumbnailUrl;
  private String keynotes;
  private String tags;
  private List<Integer> productsId;
  private Long creatorId;
}
