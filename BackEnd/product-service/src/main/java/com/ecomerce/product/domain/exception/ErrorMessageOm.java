package com.ecomerce.product.domain.exception;

public enum ErrorMessageOm {
  ACCOUNT_NOT_PERMISSION("Tài khoản không có quyền"),
  CAN_NOT_CHANGE_STATE("Không thể chuyển trạng thái"),
  IS_CURRENT_STATUS("Không thể chuyển sang trạng thái hiện tại"),
  CAN_NOT_DELETE_PRO("Không thể xóa sản phẩm"),
  PRODUCT_SELLING("Sản phẩm đang được bán"),
  STATE_IS_PRESENT("Đã là trạng thái hiện tại"),
  SHOP_ID_NOT_NULL("shopId not null"),
  SHOP_NOT_FOUND("Không tìm thấy shop"),
  SHOP_HAS_BEEN_BLOCK("Shop đang bị khóa"),
  PRODUCT_NOT_FOUND("Không tìm thấy sản phẩm");

  public String val;

  private ErrorMessageOm(String label) {
    val = label;
  }
}
