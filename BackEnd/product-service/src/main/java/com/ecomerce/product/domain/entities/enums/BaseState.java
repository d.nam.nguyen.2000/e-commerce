package com.ecomerce.product.domain.entities.enums;

public enum BaseState {
  ACTIVE,
  INACTIVE,
  DELETED
}
