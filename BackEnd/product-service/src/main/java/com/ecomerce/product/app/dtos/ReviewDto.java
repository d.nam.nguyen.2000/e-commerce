package com.ecomerce.product.app.dtos;

import lombok.Data;


@Data
public class ReviewDto {
    private Long productId;
    private Long rating;
    private String comment;
}
