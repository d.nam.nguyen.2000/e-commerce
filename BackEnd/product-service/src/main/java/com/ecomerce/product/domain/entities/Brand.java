package com.ecomerce.product.domain.entities;

import com.ecomerce.product.domain.entities.view.User;
import com.ecomerce.product.domain.utils.GenericArrayUserType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "brands")
@TypeDef(name = "pg-int-array", typeClass = GenericArrayUserType.class)
@TypeDef(name = "string-array", typeClass = StringArrayType.class)
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, length = 100)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "logo_url", length = 255)
    private String logoUrl = "https://placehold.co/296x200.png";

    @Column(name = "creator_id")
    private Long creatorId;

    @ManyToOne
    @JoinColumn(name = "creator_id", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private User creator;

    @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
    private List<Product> products;

    @Type(type = "string-array" )
    @Column(name = "keynotes")
    private String[] keynotes;

    @Type(type = "string-array" )
    @Column(name = "tags")
    private String[] tags;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private Date updatedAt;

}
