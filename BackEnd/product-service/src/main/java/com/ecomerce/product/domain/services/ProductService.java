package com.ecomerce.product.domain.services;

import com.ecomerce.product.app.dtos.ProductDto;
import com.ecomerce.product.domain.entities.*;
import com.ecomerce.product.domain.entities.enums.Actor;
import com.ecomerce.product.domain.exception.AppException;
import com.ecomerce.product.domain.repositories.BrandRepository;
import com.ecomerce.product.domain.repositories.CategoryRepository;
import com.ecomerce.product.domain.repositories.ProductRepository;
import com.ecomerce.product.domain.repositories.UserRepository;
import com.ecomerce.product.domain.utils.FnCommon;
import com.ecomerce.product.domain.utils.MessageUtils;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService extends BaseService {

  @Autowired
  private ProductRepository productRepository;
  @Autowired
  private CategoryRepository categoryRepository;
  @Autowired
  private BrandRepository brandRepository;
//  @Autowired
//  private StoreRepository storeRepository;
  @Autowired
  private UserRepository userRepository;
//  @Autowired
//  private ReviewRepository reviewRepository;

  @Transactional
  public Product addProduct(Long shopId, ProductDto requestDTO) {
    Product product = FnCommon.copyNonNullProperties(Product.class, requestDTO);
    product.setStoreId(shopId);
    if (requestDTO.getGallery() != null) {
      String[] gal = requestDTO.getGallery().split(",");
      product.setGallery(gal);
    }
    // Lưu sản phẩm vào cơ sở dữ liệu
    return productRepository.save(product);

  }

  public List<Product> getProducts() {
    return productRepository.findAll();
  }

  public List<Product> getPageProducts(String role, Long shopId, String category, String brand, String store, String actor) {
    if (actor == null || actor.equals("admin")) {
      return productRepository.findAll();
    } else if (actor.equals("seller")){
      return productRepository.findAllByStoreId(shopId);
    } else {
      return productRepository.findAll();
    }
  }

  public Product getProduct(Long id) {
    return productRepository.findById( id).orElseThrow(() -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("product_not_found")));
  }

  public List<Product> getFilteredProducts(String category, String brand, String store) {
    return productRepository.findAll();
  }

  @Transactional
  public Product updateProduct(Long shopId, Long id, ProductDto requestDTO) {
    Product product = getProduct(id);
    FnCommon.coppyNonNullProperties(product, requestDTO);
    if (requestDTO.getGallery() != null) {
      String[] gal = requestDTO.getGallery().split(",");
      product.setGallery(gal);
    }
    product.setStoreId(shopId);
    return productRepository.save(product);
  }

  @Transactional
  public void deleteProduct(Long shopId, Long id) {
    getProduct(id);
    productRepository.deleteById(id);
  }
}
