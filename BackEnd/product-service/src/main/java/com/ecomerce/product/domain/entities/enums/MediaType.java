package com.ecomerce.product.domain.entities.enums;

public enum MediaType {
  VIDEO,
  IMAGE
}
