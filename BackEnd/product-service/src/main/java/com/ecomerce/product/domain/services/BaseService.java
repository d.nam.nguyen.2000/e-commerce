package com.ecomerce.product.domain.services;

import com.ecomerce.product.domain.repositories.*;
import com.google.gson.Gson;
import com.ecomerce.product.domain.repositories.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseService {
  @Autowired
  CategoryRepository categoryRepository;
  @Autowired
  ProductRepository productRepository;
  @Autowired Gson gson;
  @Autowired ModelMapper modelMapper;
  @Autowired
  ShopRepository shopRepository;

  public static String[] convertArrStr(String jsonString) {
    Gson gson = new Gson();
    return gson.fromJson(jsonString, String[].class);
  }
}
