package com.ecomerce.product.app.controller;

import com.ecomerce.product.app.dtos.ReviewDto;
import com.ecomerce.product.domain.entities.Review;
import com.ecomerce.product.domain.services.ReviewService;
import com.ecomerce.product.domain.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/review")
public class ReviewController {
  @Autowired
  ReviewService reviewService;

  @PostMapping
  public ResponseEntity<Review> createCate(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @RequestBody @Valid ReviewDto dto) {
    return ResponseEntity.ok(reviewService.addReview(userId, dto));
  }

  @PutMapping("{id}")
  public ResponseEntity<Review> updateCate(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @PathVariable Long id, @RequestBody @Valid ReviewDto dto) {
    return ResponseEntity.ok(reviewService.updateReview(id, userId, dto));
  }

  @GetMapping()
  public ResponseEntity<List<Review>> listCate(
          @RequestHeader(name = Constant.headerUserId, required = false) Long userId,
          @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
          @RequestHeader(name = Constant.headerRole, required = false) String role) {
    return ResponseEntity.ok(reviewService.getReviews(userId, shopId, role));
  }

  @DeleteMapping("{cateId}")
  public ResponseEntity deleteCate(@PathVariable Long cateId) {
    reviewService.deleteReview(cateId);

    return ResponseEntity.ok().build();
  }

  @GetMapping("{id}")
  public ResponseEntity<Review> detailCate(@PathVariable Long id) {
    return ResponseEntity.ok(reviewService.getReviewById(id));
  }
}
