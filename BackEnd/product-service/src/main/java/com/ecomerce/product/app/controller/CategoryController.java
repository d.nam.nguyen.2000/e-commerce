package com.ecomerce.product.app.controller;

import com.ecomerce.product.app.dtos.CategoryDto;
import com.ecomerce.product.app.response.ResponsePage;
import com.ecomerce.product.domain.entities.Category;
import com.ecomerce.product.domain.services.CategoryService;
import com.ecomerce.product.domain.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("category")
public class CategoryController {
  @Autowired CategoryService categoryService;

  @PostMapping
  public ResponseEntity<Category> createCate(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @RequestBody @Valid CategoryDto dto) {
    return ResponseEntity.ok(categoryService.addCategory(userId, dto));
  }

  @PutMapping("{cateId}")
  public ResponseEntity<Category> updateCate(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @PathVariable Long cateId, @RequestBody @Valid CategoryDto dto) {
    return ResponseEntity.ok(categoryService.updateCategory(cateId, userId, dto));
  }

//  @GetMapping()
//  public ResponseEntity<ResponsePage<Category>> listCate(
//      @SortDefault(direction = Sort.Direction.ASC, value = "id") Pageable pageable,
//      @RequestParam Optional<String> keyword) {
//    return ResponseEntity.ok(new ResponsePage<>(categoryService.getListUser(keyword, pageable)));
//  }

  @GetMapping()
  public ResponseEntity<List<Category>> listCate(
          @RequestHeader(name = Constant.headerUserId, required = false) Long userId,
          @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
          @RequestHeader(name = Constant.headerRole, required = false) String role,
          @RequestParam(required = false) String actor) {
    return ResponseEntity.ok(categoryService.getCategories(userId, shopId, role, actor));
  }

  @DeleteMapping("{cateId}")
  public ResponseEntity deleteCate(@PathVariable Long cateId) {
    categoryService.deleteCategory(cateId);

    return ResponseEntity.ok().build();
  }

  @GetMapping("{cateId}")
  public ResponseEntity<Category> detailCate(@PathVariable Long cateId) {
    return ResponseEntity.ok(categoryService.getCategory(cateId));
  }
}
