package com.ecomerce.product.domain.repositories;

import com.ecomerce.product.domain.entities.enums.StoreState;
import com.ecomerce.product.domain.entities.view.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ShopRepository extends JpaRepository<Shop, Long> {

}
