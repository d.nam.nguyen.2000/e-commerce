package com.ecomerce.product.app.controller;

import com.ecomerce.product.app.dtos.ProductDto;
import com.ecomerce.product.app.response.ResponsePage;
import com.ecomerce.product.domain.entities.Product;
import com.ecomerce.product.domain.entities.enums.Actor;
import com.ecomerce.product.domain.services.ProductService;
import com.ecomerce.product.domain.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("product")
public class ProductController {
  @Autowired ProductService productService;

  /**
   * Tạo sản phẩm
   * @param shopId
   * @param isAdmin
   * @param dto
   * @return
   */
  @PostMapping
  public ResponseEntity<Product> create(
      @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
      @RequestHeader(name = Constant.headerIsAdmin, required = false) Boolean isAdmin,
      @RequestBody @Valid ProductDto dto) {
    isAdmin = isAdmin != null ? isAdmin : false;
    return ResponseEntity.ok(productService.addProduct(shopId, dto));
  }

  @PutMapping("/{id}")
  public ResponseEntity<Product> update(
      @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
      @RequestHeader(name = Constant.headerUserId) Long userId,
      @RequestHeader(name = Constant.headerIsAdmin, required = false) Boolean isAdmin,
      @Valid @RequestBody ProductDto dto,
      @PathVariable Long id) {
    isAdmin = isAdmin != null ? isAdmin : false;
    return ResponseEntity.ok(productService.updateProduct(shopId, id, dto));
  }

  /**
   * Chi tiết sản phẩm
   * @param id
   * @return
   */
  @GetMapping("/{id}")
  public ResponseEntity<Product> detail(
          @PathVariable Long id,
          @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
          @RequestHeader(name = Constant.headerIsAdmin, required = false) Boolean isAdmin,
          @Valid @RequestParam(required = false) Actor actor) {
    return ResponseEntity.ok(productService.getProduct( id));
  }
  @GetMapping()
  public ResponseEntity<List<Product>> listProduct(
          @SortDefault(direction = Sort.Direction.ASC, value = "id") Pageable pageable,
          @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
          @RequestHeader(name = Constant.headerRole, required = false) String role,
          @RequestParam(required = false) String category,
          @RequestParam(required = false) String brand,
          @RequestParam(required = false) String store,
          @RequestParam(required = false) String actor) {
    return ResponseEntity.ok(productService.getPageProducts(role, shopId, category, brand, store, actor));
  }

  @DeleteMapping("{id}")
  public ResponseEntity deleteCate(
          @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
          @PathVariable Long id) {
    productService.deleteProduct(shopId, id);

    return ResponseEntity.ok().build();
  }
}
