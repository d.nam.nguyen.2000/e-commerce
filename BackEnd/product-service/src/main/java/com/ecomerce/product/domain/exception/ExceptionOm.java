package com.ecomerce.product.domain.exception;

import org.springframework.http.HttpStatus;

public class ExceptionOm extends RuntimeException {
  HttpStatus status;

  public ExceptionOm(HttpStatus status, ErrorMessageOm msg) {
    super(msg.val);
    this.status = status;
  }

  public ExceptionOm(HttpStatus status, ErrorMessageOm msg, String data) {
    super(msg.val + "(" + data + ")");
    this.status = status;
  }

  public ExceptionOm(HttpStatus status, String msg) {
    super(msg);
    this.status = status;
  }
}
