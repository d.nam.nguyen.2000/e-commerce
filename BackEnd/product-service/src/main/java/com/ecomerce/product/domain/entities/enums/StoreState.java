package com.ecomerce.product.domain.entities.enums;

public enum StoreState {
  ACTIVE,
  INACTIVE,
  PENDING,
  DELETED,
  BLOCK,
  REJECT
}
