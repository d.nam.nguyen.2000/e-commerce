package com.ecomerce.product.app.dtos;


import lombok.Data;


@Data
public class ProductDto {

  private String title;
  private String summary;
  private String thumbnailUrl;
  private String gallery;
  private Object features;
  private Object variations;
  private Object campaign;
  private String campaignTitle;
  private String campaignState;
  private Double price;
  private Long categoryId;
  private Long brandId;

}
