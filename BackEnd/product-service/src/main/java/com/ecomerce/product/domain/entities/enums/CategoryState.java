package com.ecomerce.product.domain.entities.enums;

public enum CategoryState {
  ACTIVE,
  INACTIVE,
  DELETED
}
