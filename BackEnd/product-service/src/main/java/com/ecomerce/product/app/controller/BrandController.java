package com.ecomerce.product.app.controller;

import com.ecomerce.product.app.dtos.BrandDto;
import com.ecomerce.product.app.dtos.CategoryDto;
import com.ecomerce.product.app.response.ResponsePage;
import com.ecomerce.product.domain.entities.Brand;
import com.ecomerce.product.domain.entities.Category;
import com.ecomerce.product.domain.services.BrandService;
import com.ecomerce.product.domain.services.CategoryService;
import com.ecomerce.product.domain.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("brand")
public class BrandController {
  @Autowired
  BrandService brandService;

  @PostMapping
  public ResponseEntity<Brand> createCate(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @RequestBody @Valid BrandDto dto) {
    return ResponseEntity.ok(brandService.addBrand(userId, dto));
  }

  @PutMapping("{id}")
  public ResponseEntity<Brand> updateCate(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @PathVariable Long id, @RequestBody @Valid BrandDto dto) {
    return ResponseEntity.ok(brandService.updateBrand(id, userId, dto));
  }

  @GetMapping()
  public ResponseEntity<List<Brand>> listCate(
          @RequestHeader(name = Constant.headerUserId, required = false) Long userId,
          @RequestHeader(name = Constant.headerShopId, required = false) Long shopId,
          @RequestHeader(name = Constant.headerRole, required = false) String role,
          @RequestParam(required = false) String actor) {
    return ResponseEntity.ok(brandService.getBrands(userId, shopId, role, actor));
  }

  @DeleteMapping("{cateId}")
  public ResponseEntity deleteCate(@PathVariable Long cateId) {
    brandService.deleteBrand(cateId);

    return ResponseEntity.ok().build();
  }

  @GetMapping("{id}")
  public ResponseEntity<Brand> detailCate(@PathVariable Long id) {
    return ResponseEntity.ok(brandService.getBrandById(id));
  }
}
