package com.ecomerce.product.domain.services;

import com.ecomerce.product.app.dtos.BrandDto;
import com.ecomerce.product.domain.entities.Brand;
import com.ecomerce.product.domain.entities.Category;
import com.ecomerce.product.domain.exception.AppException;
import com.ecomerce.product.domain.repositories.BrandRepository;
import com.ecomerce.product.domain.utils.FnCommon;
import com.ecomerce.product.domain.utils.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandService extends BaseService{
    @Autowired
    private BrandRepository brandRepository;

    public Brand addBrand(Long userId, BrandDto brandDto) {
        Brand brand = FnCommon.copyNonNullProperties(Brand.class, brandDto);
        brand.setCreatorId(userId);
        if (brandDto.getKeynotes() != null)
            brand.setKeynotes(convertArrStr(brandDto.getKeynotes()));
        if (brandDto.getTags() != null)
            brand.setTags(convertArrStr(brandDto.getTags()));
        return brandRepository.save(brand);
    }

    public List<Brand> getBrands(Long userId, Long shopId, String role, String actor) {
        if (actor == null || actor.equals("admin") || actor.equals("buyer")) {
            return brandRepository.findAll();
        } else {
            return brandRepository.findAllByCreatorId(userId);
        }

    }

    public Brand getBrandById(Long id) {
        return brandRepository.findById(id)
                .orElseThrow(() -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("brand_not_found")));
    }

    public Brand updateBrand(Long id, Long userId, BrandDto dto) {
        Brand brand = getBrandById(id);
        FnCommon.coppyNonNullProperties(brand, dto);
        if (dto.getKeynotes() != null)
            brand.setKeynotes(convertArrStr(dto.getKeynotes()));
        if (dto.getTags() != null)
            brand.setTags(convertArrStr(dto.getTags()));
        return brandRepository.save(brand);
    }

    public void deleteBrand(Long id) {
        Brand brand = getBrandById(id);
        brandRepository.delete(brand);
    }
}
