package com.ecomerce.product.domain.utils;

public class Constant {
    public static final String headerShopId = "shop-id";
    public static final String headerIsAdmin = "is-admin";
    public static final String headerUserId = "user-id";
    public static final String headerName = "name";
    public static final String headerUsername = "username";
    public static final String headerRole = "role";
}
