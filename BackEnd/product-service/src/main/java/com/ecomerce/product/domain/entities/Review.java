package com.ecomerce.product.domain.entities;

import com.ecomerce.product.domain.entities.view.User;
import com.ecomerce.product.domain.utils.GenericArrayUserType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "reviews")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "reviewer_id")
    private Long reviewerId;

    @ManyToOne
    @JoinColumn(name = "reviewer_id", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private User reviewer;

    @Column(name = "product_id")
    private Long productId;

    @ManyToOne
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnoreProperties("reviews")
    private Product product;

    private Long rating = 1L;

    private String comment;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private Date updatedAt;

}
