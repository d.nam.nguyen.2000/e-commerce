package com.ecomerce.product.domain.repositories;

import com.ecomerce.product.domain.entities.enums.ProductState;
import com.ecomerce.product.domain.entities.enums.StoreState;
import com.ecomerce.product.domain.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
  Optional<Product> findByStoreIdAndId(Long shopId, Long id);
  List<Product> findAllByStoreId(Long shopId);
}
