package com.ecomerce.product.domain.services;

import com.ecomerce.product.app.dtos.CategoryDto;
import com.ecomerce.product.domain.repositories.CategoryRepository;
import com.ecomerce.product.domain.repositories.UserRepository;
import com.ecomerce.product.domain.entities.Category;
import com.ecomerce.product.domain.exception.AppException;
import com.ecomerce.product.domain.utils.FnCommon;
import com.ecomerce.product.domain.utils.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class CategoryService extends BaseService {

  @Autowired
  private CategoryRepository categoryRepository;
  @Autowired
  private UserRepository userRepository;

  @Transactional
  public Category addCategory(Long userId, CategoryDto requestDTO) {
    Category category = FnCommon.copyNonNullProperties(Category.class, requestDTO);
    category.setCreatorId(userId);
    if (requestDTO.getKeynotes() != null)
      category.setKeynotes(convertArrStr(requestDTO.getKeynotes()));
    if (requestDTO.getTags() != null)
      category.setTags(convertArrStr(requestDTO.getTags()));
    return categoryRepository.save(category);

    // Update user's category reference
//    user.setCategory(category);
//    userRepository.save(user);
  }

  public List<Category> getCategories(Long userId, Long shopId, String role, String actor) {
    if (actor == null || actor.equals("admin")|| actor.equals("buyer")) {
      return categoryRepository.findAll();
    } else {
      return categoryRepository.findAllByCreatorId(userId);
    }
  }

  public Category getCategory(Long id) {
    return categoryRepository.findById(id).orElseThrow(
            () -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("category_not_found")));
  }

  @Transactional
  public Category updateCategory(Long id, Long userId, CategoryDto requestDTO) {
    Category category = getCategory(id);

    FnCommon.coppyNonNullProperties(category, requestDTO);
    if (requestDTO.getKeynotes() != null)
      category.setKeynotes(convertArrStr(requestDTO.getKeynotes()));
    if (requestDTO.getTags() != null)
      category.setTags(convertArrStr(requestDTO.getTags()));
    return categoryRepository.save(category);
  }

  public Page<Category> getListUser(Optional<String> keyword, Pageable pageable) {
    return categoryRepository.findAll(pageable);
  }

  @Transactional
  public void deleteCategory(Long id) {
    Category category = categoryRepository.findById(id).orElse(null);
    categoryRepository.deleteById(id);
  }
}
