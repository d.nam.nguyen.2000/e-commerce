package com.ecomerce.product.domain.entities.enums;

public enum ProductState {
  DRAFT,
  PENDING,
  PUBLIC,
  REJECT,
  DELETED,
  REPORTED
}
