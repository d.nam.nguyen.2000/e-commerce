package com.ecomerce.product.domain.services;

import com.ecomerce.product.domain.entities.view.User;
import com.ecomerce.product.domain.exception.AppException;
import com.ecomerce.product.domain.repositories.UserRepository;
import com.ecomerce.product.domain.utils.MessageUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UserRepository userRepository;


    public User getUserById(Long userId) {
        return userRepository.findById(userId).orElseThrow(
                () -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("user_not_found")));
    }
}
