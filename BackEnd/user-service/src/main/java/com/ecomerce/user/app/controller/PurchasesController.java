package com.ecomerce.user.app.controller;

import com.ecomerce.user.app.dtos.ChangeStatePurchasesDto;
import com.ecomerce.user.app.dtos.PurchasesDto;
import com.ecomerce.user.entities.Purchases;
import com.ecomerce.user.service.PurchasesService;
import com.ecomerce.user.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("user/purchase")
public class PurchasesController {
  @Autowired PurchasesService PurchasesService;

  @GetMapping()
  public ResponseEntity<List<Purchases>> getPurchases(
      @RequestHeader(name = Constant.headerUserId) Long userId,
      @RequestHeader(name = Constant.headerRole, required = false) String role) {

    return ResponseEntity.ok(PurchasesService.getPurchases(userId, role));
  }

  @PostMapping()
  public ResponseEntity<Purchases> createPurchases(
      @RequestHeader(name = Constant.headerUserId) Long userId,
      @RequestBody @Valid List<PurchasesDto> dto) {
    return ResponseEntity.ok(PurchasesService.addToPurchases(userId, dto));
  }

  @PutMapping()
  public ResponseEntity<List<Purchases>> updatePurchases(
      @RequestHeader(name = Constant.headerUserId) Long userId,
      @RequestBody @Valid List<PurchasesDto> dto) {
    return ResponseEntity.ok(PurchasesService.updatePurchases(userId, dto));
  }

  @PutMapping("/{id}")
  public ResponseEntity updatePurchases1(
      @PathVariable Long id, @RequestBody ChangeStatePurchasesDto dto) {
    PurchasesService.updatePurchases1(id, dto);
    return ResponseEntity.ok().build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deletePurchases(
      @PathVariable Long id, @RequestHeader(name = Constant.headerUserId) Long userId) {
    PurchasesService.deletePurchases(id, userId);
    return ResponseEntity.ok().build();
  }
}
