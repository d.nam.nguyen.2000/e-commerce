package com.ecomerce.user.service;

import com.ecomerce.user.repositories.*;
import com.ecomerce.user.repositories.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BaseService {
  @Autowired
  ProductRepository productRepository;
  @Autowired
  CartRepository cartRepository;
  @Autowired ModelMapper modelMapper;
  @Autowired
  UserRepository userRepository;
  @Autowired
  ProvinceRepository provinceRepository;
  @Autowired
  DistrictRepository districtRepository;
  @Autowired
  WardRepository wardRepository;
}
