package com.ecomerce.user.service;

import com.ecomerce.user.entities.views.Product;
import com.ecomerce.user.exceptions.AppException;
import com.ecomerce.user.utils.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductService extends BaseService {

  public Product getProduct(Long id) {
    return productRepository
        .findById(id)
        .orElseThrow(
            () -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("product_not_found")));
  }
}
