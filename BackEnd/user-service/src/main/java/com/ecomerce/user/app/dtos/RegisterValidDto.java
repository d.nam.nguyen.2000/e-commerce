package com.ecomerce.user.app.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RegisterValidDto {
  private String email;

  @NotNull private String phone;

  @NotNull private String password;
}
