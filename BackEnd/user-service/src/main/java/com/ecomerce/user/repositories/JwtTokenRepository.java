package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.JwtToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface JwtTokenRepository extends JpaRepository<JwtToken, Long> {
  boolean existsByHashToken(String hashToken);

  void deleteByUserId(long userId);

  void deleteBySessionId(String sessionId);

  @Modifying
  @Query(value = "delete from jwt_token where expiration_at < now()", nativeQuery = true)
  void deleteExpirationToken();
}
