package com.ecomerce.user.app.controller;

import com.ecomerce.user.app.dtos.ForgotPasswordDto;
import com.ecomerce.user.app.dtos.LoginDto;
import com.ecomerce.user.app.dtos.RefresTokenDto;
import com.ecomerce.user.app.dtos.RegisterDto;
import com.ecomerce.user.app.response.LoginRes;
import com.ecomerce.user.app.response.RegisterRes;
import com.ecomerce.user.service.AuthService;
import com.ecomerce.user.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("auth")
public class AuthController {
  @Autowired
  AuthService authService;

  @PostMapping(value = "/sign-up")
  public RegisterRes register(@RequestBody @Valid RegisterDto dto) {
    return authService.register(dto);
  }

  @PostMapping(value = "/sign-in")
  public LoginRes login(@RequestBody @Valid LoginDto dto) {
    return authService.login(dto);
  }

  @PostMapping(
      value = "gettoken",
      params = {"service"})
  public LoginRes gettoken(
      @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
      @RequestBody(required = false) Map<String, Object> metadata) {
    return authService.gettoken(token, metadata);
  }

  @PutMapping("/forgot-password")
  public ResponseEntity forgotPassword(@RequestBody ForgotPasswordDto dto) {
    return ResponseEntity.ok(authService.fordgotPass(dto));
  }
  @RequestMapping("/valid-token")
  public ResponseEntity validToken(
      @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
    return ResponseEntity.ok(authService.validToken(token));
  }

  @DeleteMapping("/logout")
  public void logout(
      @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
      @RequestParam Optional<Boolean> logoutAll) {
    authService.logout(token, logoutAll);
  }
}
