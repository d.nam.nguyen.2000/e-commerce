package com.ecomerce.user.app.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ChangePasswordDto {
  @NotNull
  private String oldPassword;

  @NotNull
  private String newPassword;
}
