package com.ecomerce.user.app.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class FavoriteDto {
  @NotNull private Long productId;
}
