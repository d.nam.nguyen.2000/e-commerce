package com.ecomerce.user.security;

import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Log4j2
@Component
public class JwtUtils {
  @Value("${sso.jwt-secret}")
  public String tokenSecret;

  @Value("${sso.jwt-refresh-secret}")
  public String refreshTokenSecret;

  public static String replaceUtf8(String input) {
    if (input == null) {
      return "";
    }
    String slug =
        input
            .replaceAll("[àáảãạăắằẳẵặâấầẩẫậ]", "a")
            .replaceAll("[đ]", "d")
            .replaceAll("[èéẻẽẹêềếểễệ]", "e")
            .replaceAll("[ìíỉĩị]", "i")
            .replaceAll("[òóỏõọôồốổỗộơờớởỡợ]", "o")
            .replaceAll("[ùúủũụưừứửữự]", "u")
            .replaceAll("[ỳýỷỹỵ]", "y");

    return slug;
  }

  public String generateJwtToken(Map<String, Object> claim, TokenType type, long expriAfter) {

    return Jwts.builder()
        .setIssuedAt(new Date())
        .setExpiration(new Date(System.currentTimeMillis() + expriAfter))
        .addClaims(formatClaim(claim))
        .signWith(SignatureAlgorithm.HS512, type == TokenType.access ? tokenSecret : refreshTokenSecret)
        .compact();
  }

  public String generateJwtToken(Map<String, Object> claim, TokenType type, Date expriAt) {

    return Jwts.builder()
        .setIssuedAt(new Date())
        .setExpiration(expriAt)
        .addClaims(formatClaim(claim))
        .signWith(SignatureAlgorithm.HS512, type == TokenType.access ? tokenSecret : refreshTokenSecret)
        .compact();
  }

  /**
   * Xoá tiếng việt và loại bỏ value dài
   *
   * @param claim
   * @return
   */
  private Map<String, Object> formatClaim(Map<String, Object> claim) {
    if (!claim.containsKey("user-id")
        || !claim.containsKey("rexp")) {
      throw new RuntimeException("Jwt claim required user-id and rexp");
    }

    Map<String, Object> newClaim = new HashMap<>();
    for (String s : claim.keySet()) {
      if (claim.get(s) != null && claim.get(s) instanceof String) {
        if (claim.get(s).toString().length() <= 36) {
          newClaim.put(s, replaceUtf8((String) claim.get(s)));
        }
        continue;
      }
      newClaim.put(s, claim.get(s));
    }

    return newClaim;
  }

  public Long getUserId(String token, TokenType type) {
    return Long.valueOf(
        Jwts.parser()
            .setSigningKey(type == TokenType.access ? tokenSecret : refreshTokenSecret)
            .parseClaimsJws(removeBear(token))
            .getBody()
            .get("user-id")
            .toString());
  }

  /**
   * @param token
   * @return milliseconds
   */
  public Long getTokenExpiration(String token, TokenType type) {
    return Long.parseLong(
            Jwts.parser()
                .setSigningKey(type == TokenType.access ? tokenSecret : refreshTokenSecret)
                .parseClaimsJws(removeBear(token))
                .getBody()
                .get("exp")
                .toString())
        * 1000;
  }

  public Claims claimAll(String token, TokenType type) {
    return Jwts.parser().setSigningKey(type == TokenType.access ? tokenSecret : refreshTokenSecret).parseClaimsJws(removeBear(token)).getBody();
  }

  public boolean validateJwtToken(String authToken, TokenType type) {
    if (authToken == null) {
      return false;
    }
    try {
      Jwts.parser().setSigningKey(type == TokenType.access ? tokenSecret : refreshTokenSecret).parseClaimsJws(removeBear(authToken));
      return true;
    } catch (SignatureException e) {
      log.error("Invalid JWT signature: {}", e.getMessage());
    } catch (MalformedJwtException e) {
      log.error("Invalid JWT token: {}", e.getMessage());
    } catch (ExpiredJwtException e) {
      log.error("JWT token is expired: {}", e.getMessage());
    } catch (UnsupportedJwtException e) {
      log.error("JWT token is unsupported: {}", e.getMessage());
    } catch (IllegalArgumentException e) {
      log.error("JWT claims string is empty: {}", e.getMessage());
    }

    return false;
  }

  public String removeBear(String bearToken) {

    if (StringUtils.hasText(bearToken) && bearToken.startsWith("Bearer ")) {
      return bearToken.substring(7, bearToken.length());
    }

    return bearToken;
  }

  public static enum TokenType{
    access,
    reftoken
  }
}
