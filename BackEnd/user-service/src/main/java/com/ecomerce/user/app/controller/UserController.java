package com.ecomerce.user.app.controller;

import com.ecomerce.user.app.dtos.StoreChangeStateDto;
import com.ecomerce.user.app.dtos.UserDto;
import com.ecomerce.user.app.response.ResponsePage;
import com.ecomerce.user.entities.User;
import com.ecomerce.user.service.UserService;
import com.ecomerce.user.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserController {
  @Autowired UserService userService;

  @GetMapping("me")
  public User getUserMe(@RequestHeader(name = Constant.headerUserId) Long userId) {
    return userService.getUserById(userId);
  }

  /**
   * update or create profile
   *
   * @param userId
   * @param dto
   * @return
   */
  @PutMapping()
  public User updateUser(
      @RequestHeader(name = Constant.headerUserId) Long userId, @RequestBody UserDto dto) {
    return userService.updateUser(userId, dto);
  }

  @PutMapping("/{id}")
  public User updateUserById(
          @PathVariable Long id, @RequestBody UserDto dto) {
    return userService.updateUser(id, dto);
  }

  @PutMapping("/seller-review")
  public User registerSeller(
          @RequestParam Long id, @RequestBody StoreChangeStateDto dto) {
    return userService.registerSeller(id, dto);
  }

  @GetMapping("/seller-review")
  public List<User> getRegisterSeller() {
    return userService.getRegisterSeller();
  }

  @GetMapping()
  public List<User> getListUser(
          @RequestParam Optional<String> keyword,
          Pageable pageable) {
    return userService.getListUser(keyword, pageable);
  }

  @GetMapping("{userId}")
  public User getUserById(@PathVariable Long userId) {
    return userService.getUserById(userId);
  }
}
