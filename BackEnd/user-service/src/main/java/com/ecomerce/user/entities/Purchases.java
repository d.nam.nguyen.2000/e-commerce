package com.ecomerce.user.entities;

import com.ecomerce.user.entities.views.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "purchases")
public class Purchases {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @JsonIgnore
  @Column(name = "customer_id")
  private Long customerId;

  @Column(name = "store_id")
  private Long storeId;

  
  @NotFound(action = NotFoundAction.IGNORE)
@ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", insertable = false, updatable = false)
  @ToString.Exclude
  @JsonIgnoreProperties({"purchases", "cart", "reviews"})
  private User customer;

  @Type(type = "pg-jsonb")
  private List<ProQty> products;

  private String status;

  @Column(name = "total_amount")
  private Double totalAmount;

  @CreationTimestamp
  @Column(name = "created_at")
  private Timestamp createdAt;

  @Data
  public static class ProQty {
    private Product product;
    private Integer quantity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Purchases purchases = (Purchases) o;
    return id != null && Objects.equals(id, purchases.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
