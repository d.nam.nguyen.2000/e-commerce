package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.Cart;
import com.ecomerce.user.entities.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite, Long> {

  List<Favorite> findByUserId(long uId);

}
