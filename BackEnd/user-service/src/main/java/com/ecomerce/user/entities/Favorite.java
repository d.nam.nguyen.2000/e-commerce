package com.ecomerce.user.entities;

import com.ecomerce.user.entities.views.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "favorites")
public class Favorite {
@ManyToOne()
  @JoinColumn(name = "product_id", insertable = false, updatable = false)
  @NotFound(action = NotFoundAction.IGNORE)
  Product product;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @JsonIgnore
  @Column(name = "product_id")
  private Long productId;

  @JsonIgnore
  @Column(name = "user_id")
  private Long userId;

  @CreationTimestamp
  @Column(name = "created_at")
  private Timestamp createdAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Favorite favorite = (Favorite) o;
    return id != null && Objects.equals(id, favorite.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
