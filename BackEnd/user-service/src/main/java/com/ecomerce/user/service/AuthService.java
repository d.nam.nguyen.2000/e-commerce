package com.ecomerce.user.service;

import com.ecomerce.user.app.dtos.*;
import com.ecomerce.user.app.response.LoginRes;
import com.ecomerce.user.app.response.RegisterRes;
import com.ecomerce.user.entities.User;
import com.ecomerce.user.entities.views.Shop;
import com.ecomerce.user.exceptions.ErrorMessageOm;
import com.ecomerce.user.exceptions.ExceptionOm;
import com.ecomerce.user.exceptions.ExceptionResponse;
import com.ecomerce.user.repositories.ShopRepository;
import com.ecomerce.user.repositories.UserRepository;
import com.ecomerce.user.security.JwtUtils;
import com.ecomerce.user.security.JwtUtils.TokenType;
import com.ecomerce.user.utils.*;
import com.google.gson.Gson;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class AuthService {
  private final Map<String, String> mapServiceClaimApi = new HashMap<>();

  public Long tokenExpMs = 24 * 60 * 60000L;
  //  public Long tokenExpMs = 30 * 60000L;

  @Value("${sso.jwt-refresh-expiration-ms}")
  public Long refreshTokenExpMs;

  @Autowired UserRepository userRepository;
  @Autowired UserService userService;
  @Autowired JwtUtils jwtUtils;
  @Autowired ShopRepository shopRepository;

  @Value("${sso.jwt-secret}")
  private String ssoSecretToken;

  public void deleteAccount(String token) {
    if (!isValidToken(TokenType.access, token)) {
      throw new ExceptionOm(HttpStatus.UNAUTHORIZED, ErrorMessageOm.UNAUTHORIZED);
    }
    Claims refreshClaim = jwtUtils.claimAll(token, TokenType.access);
    String service =
        refreshClaim.get("service") == null ? null : refreshClaim.get("service", String.class);
    if (service == null) {
      return;
    }
    Long userId = jwtUtils.getUserId(token, TokenType.access);
    User user =
        userRepository
            .findById(userId)
            .orElseThrow(() -> new ExceptionOm(HttpStatus.NOT_FOUND, "Không tìm thấy người dùng"));
    userRepository.save(user);

    logout(token, Optional.of(true));
  }

  @Transactional(rollbackFor = Exception.class)
  public RegisterRes register(@Validated RegisterDto dto) {
    if (userRepository.existsByPhone(dto.getPhone())
        || userRepository.existsByEmail(dto.getEmail())) {
      throw new ExceptionOm(HttpStatus.BAD_REQUEST, ErrorMessageOm.USER_EXISTS);
    }
    User user = FnCommon.copyProperties(User.class, dto);
    user.setPassword(PasswordUtils.encode(dto.getPassword()));
    user.setStatus("active");
    userRepository.save(user);

    RegisterRes res = FnCommon.copyProperties(RegisterRes.class, user);
    return res;
  }

  @Transactional(rollbackFor = Exception.class)
  public LoginRes login(@Validated LoginDto dto) {
    User user =
        userRepository
            .findByEmailOrName(dto.getUsername(), dto.getUsername())
            .orElseThrow(() -> new ExceptionOm(HttpStatus.NOT_FOUND, "Không tìm thấy người dùng"));

    if (!PasswordUtils.verifyPassword(dto.getPassword(), user.getPassword())) {
      throw new ExceptionOm(HttpStatus.BAD_REQUEST, "Sai mật khẩu");
    }

    Map<String, Object> claims = getLoginClaims(user.getId(), new HashMap<>());
    // thời điểm refresh token hết hạn
    claims.put("rexp", System.currentTimeMillis() + refreshTokenExpMs);
    // token
    String token = jwtUtils.generateJwtToken(claims, TokenType.access, tokenExpMs);
    // refresh token
    String refresh = jwtUtils.generateJwtToken(claims, TokenType.reftoken, refreshTokenExpMs);

    return new LoginRes(token, refresh, claims);
  }

  public LoginRes gettoken(String token, Map<String, Object> metadata) {
    if (!isValidToken(TokenType.access, token)) {
      throw new ExceptionOm(HttpStatus.UNAUTHORIZED, ErrorMessageOm.UNAUTHORIZED);
    }
    Claims tokenClaim = jwtUtils.claimAll(token, TokenType.access);
    Map<String, Object> claims =
        getLoginClaims(Long.parseLong(tokenClaim.get("user-id") + ""), metadata);
    claims.put("rexp", tokenClaim.get("rexp"));
    String newToken = jwtUtils.generateJwtToken(claims, TokenType.access, tokenExpMs);

    String refresh =
        jwtUtils.generateJwtToken(
            claims, TokenType.reftoken, new Date(Long.parseLong(tokenClaim.get("rexp") + "")));

    return new LoginRes(newToken, refresh, claims);
  }

  private String getTokenKey(TokenType type, String token) {
    if (!jwtUtils.validateJwtToken(token, type)) {
      throw new ExceptionOm(HttpStatus.UNAUTHORIZED, ErrorMessageOm.UNAUTHORIZED);
    }
    Claims claims = jwtUtils.claimAll(token, type);

    return String.format(
        "token:%s:%s_%s_%s:%s",
        type,
        claims.get("service"),
        claims.get("service"),
        claims.get("user-id"),
        HashUtil.getMD5(jwtUtils.removeBear(token)));
  }

  private String getTokenPrefix(TokenType type, String token) {
    if (!jwtUtils.validateJwtToken(token, TokenType.access)) {
      throw new ExceptionOm(HttpStatus.UNAUTHORIZED, ErrorMessageOm.UNAUTHORIZED);
    }
    Claims claims = jwtUtils.claimAll(token, type);

    return String.format(
        "token:%s:%s_%s_%s",
        type, claims.get("service"), claims.get("service"), claims.get("user-id"));
  }

  /**
   * check author
   *
   * @param token
   * @param roles
   * @return 200|401|403
   */
  public Integer checkAuthorization(String token, Set<String> roles) {
    try {
      Map<String, Object> claim = new HashMap<>();
      if (claim == null) {
        return 401;
      }
      boolean hasRole =
          Arrays.stream(claim.get("roles").toString().split(",")).anyMatch(roles::contains);

      return hasRole || Boolean.parseBoolean(claim.get("owner").toString()) ? 200 : 403;
    } catch (Exception e) {
    }
    return 401;
  }

  private Boolean isValidToken(TokenType type, String token) {
    return null;
  }

  public Object validToken(String token) {
    try {
      Map<String, Object> res = new HashMap<>();
      //      if (!isValidToken(TokenType.access, token)) {
      //        res.put("authenticate", false);
      //
      //        return res;
      //      }
      Claims claims = jwtUtils.claimAll(token, TokenType.access);
      claims.put("authenticate", true);
      claims.put("Authorization", token);

      return claims;
    } catch (Exception ignored) {
    }
    Map<String, Object> res = new HashMap<>();
    res.put("authenticate", false);

    return res;
  }

  public void logout(String token, Optional<Boolean> logoutAll) {}

  private Map<String, Object> getLoginClaims(long userId, Map<String, Object> metadata) {
    User user = userRepository.findById(userId).orElse(new User());
    //    Optional<Shop> shop = shopRepository.findByOwnerId(userId);
    GetTokenDto req = FnCommon.copyNonNullProperties(GetTokenDto.class, user);
    if (req != null) {
      req.setToken(ssoSecretToken);
    }
    Map<String, Object> claims = new HashMap<>();
    // Get claim from destination service

    claims.put("user-id", userId);
    claims.put("name", user.getName());
    claims.put("email", user.getName());
    claims.put("avatar", user.getAvatarUrl());
    claims.put("role", user.getRole());
    claims.put("status", user.getStatus());
    claims.put("shop-id", user.getStore() != null ? user.getStore().getId() : "");

    return claims;
  }

  public User fordgotPass(ForgotPasswordDto dto) {
    User user = userService.getUserByEmail(dto.getEmail());
    user.setPassword(PasswordUtils.encode(dto.getPassword()));
    return userRepository.save(user);
  }
}
