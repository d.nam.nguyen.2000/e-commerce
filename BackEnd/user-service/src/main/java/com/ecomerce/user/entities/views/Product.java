package com.ecomerce.user.entities.views;

import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "products")
@TypeDef(name = "pg-string-array", typeClass = StringArrayType.class)
@TypeDef(name = "pg-jsonb", typeClass = JsonBinaryType.class)
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "title", length = 100, nullable = false)
  private String title;

  @Column(name = "summary", columnDefinition = "TEXT", nullable = false)
  private String summary;

  @Column(name = "thumbnail_url", length = 255, nullable = false)
  private String thumbnailUrl;

  @Type(type = "string-array" )
  @Column(name = "gallery")
  private String[] gallery;

  @Type(type = "pg-jsonb" )
  @Column(name = "features")
  private Object features;

  @Type(type = "pg-jsonb" )
  @Column(name = "campaign")
  private Object campaign;

  @Type(type = "pg-jsonb")
  @Column(name = "variations")
  private Object variations;

  @Column(name = "campaign_title", length = 100)
  private String campaignTitle;

  @Column(name = "campaign_state", length = 20)
  private String campaignState;

  @Column(name = "price", nullable = false)
  private Double price = 0.0;

  @Column(name = "category_id")
  private Long categoryId;

  @NotFound(action = NotFoundAction.IGNORE)
@ManyToOne
  @JoinColumn(name = "category_id", insertable = false, updatable = false)
  private Category category;

  @Column(name = "brand_id")
  private Long brandId;

  @NotFound(action = NotFoundAction.IGNORE)
@ManyToOne
  @JoinColumn(name = "brand_id", insertable = false, updatable = false)
  private Brand brand;

  @Column(name = "store_id", nullable = false)
  private Long storeId;

  @NotFound(action = NotFoundAction.IGNORE)
@ManyToOne
  @JoinColumn(name = "store_id", insertable = false, updatable = false)
  private Shop store;

//  @ManyToMany
//  @JoinTable(
//          name = "product_buyers",
//          joinColumns = @JoinColumn(name = "product_id"),
//          inverseJoinColumns = @JoinColumn(name = "buyer_id")
//  )
//  private List<User> buyers;

  @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
  @ToString.Exclude
  private List<Review> reviews;

  @CreationTimestamp
  @Column(name = "created_at")
  private Date createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at")
  private Date updatedAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Product product = (Product) o;
    return id != null && Objects.equals(id, product.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
