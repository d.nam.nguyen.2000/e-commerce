package com.ecomerce.user.app.dtos;

import lombok.Data;

import javax.validation.constraints.AssertTrue;
import java.util.List;

@Data
public class StoreChangeStateDto {
  private String status;
  private String role;

}
