package com.ecomerce.user.exceptions;

public enum ErrorMessageOm {
  JOB_NOT_FOUND("Không tìm thấy công việc"),
  PERMISSION_DENIED("Không có quyền truy cập"),
  MEDICINE_NOT_FOUND("Không tìm thấy nông dược"),
  FERTILIZER_NOT_FOUND("Không tìm thấy phân bón"),
  EQUIPMENT_NOT_FOUND("Không tìm thấy trang thiết bị"),
  INGREDIENT_NOT_FOUND("Không tìm thấy nguyên liệu"),
  MEDICINE_NOT_ENOUGH("Số lượng nông dược không đủ"),
  FERTILIZER_NOT_ENOUGH("Số lượng phân bón không đủ"),
  INGREDIENT_NOT_ENOUGH("Số lượng nguyên liệu không đủ"),
  EQUIPMENT_NOT_ENOUGH("Số lượng trang thiết bị không đủ"),
  PROCEDURE_NOT_FOUND("Không tìm thấy quy trình"),
  PRODUCT_NOT_FOUND("Không tìm thấy sản phẩm"),
  TOPIC_NOT_FOUND("Không tìm thấy topic"),
  PROCEDURE_JOB_NOT_FOUND("Không tìm thấy công việc"),
  PROCEDURE_JOB_NOT_ENOUGH("Danh sách công việc không khớp"),
  PROCEDURE_BATCH_NOT_FOUND("Không tìm thấy lô sản xuất"),
  JOB_NOT_COMPLETE("Chưa hoàn thành các công việc trong tiến trình"),
  STATUS_NOT_CHANGE("Không thể thay đổi trạng thái"),
  WAREHOUSE_REQUEST_NOT_FOUND("Không tìm thấy biên bản xuất nhập kho"),
  SUPPLIES_NOT_FOUND("Không tìm thấy vật tư"),
  SUCCESS("Thành công"),
  BATCH_NOT_FOUND("Không tìm thấy lô hàng"),
  BATCH_CODE_IN_USED("Mã lô đã được sử dụng"),
  PRODUCT_BATCH_NOT_FOUND("Không tìm thấy lô sản phẩm"),
  WAREHOUSE_REQUEST_NOT_ACCEPT_APPROVED("Yêu cầu đã được duyệt"),
  WAREHOUSE_REQUEST_NOT_ACCEPT_REJECT("Yêu cầu đã bị từ chối"),
  QUANTITY_NOT_ENOUGH("Số lượng không đủ"),
  SERVICE_NOT_FOUND("Dịch vụ không đúng"),
  LOGIN_AGAIN("Đã có lỗi xảy ra, vui lòng thử lại sau ít phút."),
  OLD_PASSWORD_NOT_MATCH("Mật khẩu cũ không đúng"),
  USER_EXISTS("Người dùng đã tồn tại"),
  INVALID_OTP("OTP không hợp lệ"),
  CONNECT_SMS_FAIL("Gọi sms service lỗi"),
  USER_NOT_VERIFIED("Tài khoản chưa được xác thực"),
  FAST_OTP("Bạn gửi OTP quá nhanh"),
  UNAUTHORIZED("unauthorized"),
  USER_NOT_FOUND("Không tìm thấy user");

  public String val;

  private ErrorMessageOm(String label) {
    val = label;
  }
}
