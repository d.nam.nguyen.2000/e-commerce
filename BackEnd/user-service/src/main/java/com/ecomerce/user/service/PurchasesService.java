package com.ecomerce.user.service;

import com.ecomerce.user.app.dtos.ChangeStatePurchasesDto;
import com.ecomerce.user.app.dtos.PurchasesDto;
import com.ecomerce.user.entities.Cart;
import com.ecomerce.user.entities.Purchases;
import com.ecomerce.user.entities.User;
import com.ecomerce.user.entities.views.Product;
import com.ecomerce.user.exceptions.AppException;
import com.ecomerce.user.repositories.CartRepository;
import com.ecomerce.user.repositories.PurchasesRepository;
import com.ecomerce.user.utils.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PurchasesService extends BaseService {

  @Autowired private ProductService productService;
  @Autowired private PurchasesRepository PurchasesRepository;
  @Autowired private CartRepository cartRepository;
  @Autowired private UserService userService;

  public Purchases addToPurchases(Long userId, List<PurchasesDto> list) {
    Purchases Purchases = new Purchases();
    Purchases.setCustomerId(userId);
    Purchases.setStatus("pending");
    List<com.ecomerce.user.entities.Purchases.ProQty> qties = new ArrayList<>();
    final Double[] total = {0.0};
    final Long[] shopId = new Long[1];
    list.forEach(
        d -> {
          com.ecomerce.user.entities.Purchases.ProQty qty =
              new com.ecomerce.user.entities.Purchases.ProQty();
          Product product = productService.getProduct(d.getPid());
          qty.setProduct(product);
          qty.setQuantity(d.getQuantity());
          total[0] += d.getPrice() * d.getQuantity();
          shopId[0] = product.getStoreId();

          qties.add(qty);
        });
    Purchases.setStoreId(shopId[0]);
    Purchases.setProducts(qties);
    Purchases.setTotalAmount(total[0]);
    PurchasesRepository.save(Purchases);
    // delete cart
    List<Cart> carts = cartRepository.findByUserId(userId);
    cartRepository.deleteAll(carts);
    return Purchases;
  }

  public List<Purchases> updatePurchases(Long userId, List<PurchasesDto> dto) {
    List<Purchases> Purchasess = new ArrayList<>();
    return Purchasess;
  }

  public void updatePurchases1(Long id, ChangeStatePurchasesDto dto) {
    Purchases purchases =PurchasesRepository.findById(id).orElseThrow(
            () -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("không thấy đơn hàng")));
    purchases.setStatus(dto.getStatus());
    PurchasesRepository.save(purchases);
  }

  @Transactional
  public void deletePurchases(Long id, Long userId) {
    PurchasesRepository.deleteById(id);
  }

  public List<Purchases> getPurchases(Long userId, String role) {
    User user = userService.getUserById(userId);
    if (user.getRole().equals("admin")) {
      return PurchasesRepository.findAll();
    } else if (user.getRole().equals("seller")) {
      return PurchasesRepository.findAllByStoreId(user.getStore().getId());
    } else {
      return PurchasesRepository.findByCustomerId(userId);
    }
  }
}
