package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

  List<Cart> findByUserId(long uId);
}
