package com.ecomerce.user.entities.views;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "m_district", schema = "public")
public class District {
  @Id private Long id;

  @Column(name = "province_id")
  private Long provinceId;

  private String name;
}
