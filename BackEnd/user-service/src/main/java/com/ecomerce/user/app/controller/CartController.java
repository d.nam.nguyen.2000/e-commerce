package com.ecomerce.user.app.controller;

import com.ecomerce.user.app.dtos.CartDto;
import com.ecomerce.user.entities.Cart;
import com.ecomerce.user.service.CartService;
import com.ecomerce.user.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("user/cart")
public class CartController {
  @Autowired CartService cartService;

  @GetMapping()
  public ResponseEntity<List<Cart>> getCart(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @RequestHeader(name = Constant.headerRole, required = false) String role) {

    return ResponseEntity.ok(cartService.getCart(userId, role));
  }

  @PostMapping()
  public ResponseEntity<Cart> addToCart(
      @RequestHeader(name = Constant.headerUserId) Long userId, @RequestBody @Valid CartDto dto) {
    return ResponseEntity.ok(cartService.addToCart(userId, dto));
  }

  @PutMapping()
  public ResponseEntity<List<Cart>> updateCart(
      @RequestHeader(name = Constant.headerUserId) Long userId,
      @RequestBody @Valid List<CartDto> dto) {
    return ResponseEntity.ok(cartService.updateCart(userId, dto));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteCart(
          @PathVariable Long id,
      @RequestHeader(name = Constant.headerUserId) Long userId) {
    cartService.deleteCart(id, userId);
    return ResponseEntity.ok().build();
  }
}
