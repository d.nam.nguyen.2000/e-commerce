package com.ecomerce.user.entities.enums;

public enum ProductState {
  DRAFT,
  PENDING,
  PUBLIC,
  REJECT,
  DELETED,
  REPORTED
}
