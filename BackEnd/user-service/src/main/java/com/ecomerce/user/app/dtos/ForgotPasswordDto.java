package com.ecomerce.user.app.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class ForgotPasswordDto {
  @Email private String email;

  @NotNull private String password;
}
