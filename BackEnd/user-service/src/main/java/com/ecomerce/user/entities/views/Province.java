package com.ecomerce.user.entities.views;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "m_province", schema = "public")
public class Province {
  @Id private Long id;

  private String name;
}
