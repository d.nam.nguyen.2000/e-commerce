package com.ecomerce.user.entities.views;

import com.ecomerce.user.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "reviews")
public class Review {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "reviewer_id")
  private Long reviewerId;

  @NotFound(action = NotFoundAction.IGNORE)
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "reviewer_id", insertable = false, updatable = false)
  @JsonIgnoreProperties({"purchases", "cart", "reviews"})
  @ToString.Exclude
  private User reviewer;

  @Column(name = "product_id")
  private Long productId;

  @NotFound(action = NotFoundAction.IGNORE)
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "product_id", insertable = false, updatable = false)
  @JsonIgnoreProperties("reviews")
  @ToString.Exclude
  private Product product;

  private Long rating = 1L;

  private String comment;

  @CreationTimestamp
  @Column(name = "created_at")
  private Date createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at")
  private Date updatedAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Review review = (Review) o;
    return id != null && Objects.equals(id, review.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
