package com.ecomerce.user.entities;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "jwt_token")
public class JwtToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "hash_token")
    private String hashToken;

    @Column(name = "expiration_at")
    private Timestamp expirationAt;

    @Column(name = "created_at")
    @CreationTimestamp
    private Timestamp createdAt;
}
