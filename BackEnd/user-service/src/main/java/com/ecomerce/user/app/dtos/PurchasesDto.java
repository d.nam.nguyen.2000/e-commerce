package com.ecomerce.user.app.dtos;

import com.ecomerce.user.entities.User;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PurchasesDto {
  private Long pid;
  private Long cid;
  private Integer quantity;
  private Double price;
  private Double totalAmount;
}
