package com.ecomerce.user.app.response;

import com.ecomerce.user.utils.MessageUtils;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SuccessResponse {
    private String message;

    public SuccessResponse(){
        this.message = MessageUtils.getMsg("success");
    }
}
