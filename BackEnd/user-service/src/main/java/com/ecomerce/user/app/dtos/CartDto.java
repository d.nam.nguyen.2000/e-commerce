package com.ecomerce.user.app.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CartDto {
  @NotNull private Long productId;
  private Integer quantity = 1;
}
