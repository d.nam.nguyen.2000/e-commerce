package com.ecomerce.user.app.response;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Map;

@Data
public class RegisterRes {
    private Long id;

    private String username;

    private Boolean isAdmin = false;

    private String name;

    private String password;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private String token;

    private String refreshToken;

    private Map<String,Object> claims;
}
