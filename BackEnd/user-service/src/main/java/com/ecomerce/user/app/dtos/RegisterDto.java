package com.ecomerce.user.app.dtos;

import com.ecomerce.user.entities.enums.Gender;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
public class RegisterDto {
  private String name;

  @NotNull private String password;

  @Email
  private String email;

  private String phone;

  private String avatarUrl;
  private String role;
}
