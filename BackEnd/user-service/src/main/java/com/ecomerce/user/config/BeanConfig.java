package com.ecomerce.user.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    Logger logger(){
        return LogManager.getLogger("userService");
    }

    @Bean
    ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
