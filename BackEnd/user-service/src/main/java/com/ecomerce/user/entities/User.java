package com.ecomerce.user.entities;

import com.ecomerce.user.entities.views.Review;
import com.ecomerce.user.entities.views.Shop;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
@Getter
@Setter
@RequiredArgsConstructor
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false, unique = true)
  private String email;

  @JsonIgnore
  @Column(nullable = false)
  private String password;

  @Column(nullable = false)
  private String phone;

  @Column(nullable = false)
  private String role;

  @Column(nullable = false)
  private String status;

  @Column(name = "avatar_url")
  private String avatarUrl;

  private String address;

  @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
  @ToString.Exclude
  private List<Cart> cart;

  @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
  @ToString.Exclude
  private List<Purchases> purchases;

  @OneToMany(mappedBy = "reviewer", fetch = FetchType.LAZY)
  @ToString.Exclude
  private List<Review> reviews;

  @OneToOne(mappedBy = "owner")
  private Shop store;

  @Column(nullable = false, updatable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdAt;

  @Temporal(TemporalType.TIMESTAMP)
  private Date updatedAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    User user = (User) o;
    return id != null && Objects.equals(id, user.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
