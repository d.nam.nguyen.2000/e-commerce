package com.ecomerce.user.entities.views;

import com.ecomerce.user.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "stores")
public class Shop {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "title", length = 100, nullable = false)
  private String title;

  @Column(name = "description")
  private String description;

  @Column(name = "thumbnail")
  private String thumbnail;

  @Column(name = "owner_id")
  private Long ownerId;

  @NotFound(action = NotFoundAction.IGNORE)
  @OneToOne
  @JoinColumn(name = "owner_id", insertable = false, updatable = false)
  @JsonIgnoreProperties("store")
  @ToString.Exclude
  private User owner;


//    @ElementCollection
//    @Column(name = "products_id", columnDefinition = "INTEGER[]")
//    private List<Integer> productsId;

  @Column(name = "status", length = 10, columnDefinition = "VARCHAR(10) DEFAULT 'active'")
  private String status;

//    @ElementCollection
//    @Column(name = "keynotes", columnDefinition = "TEXT[]")
//    private List<String> keynotes;

//    @ElementCollection
//    @Column(name = "tags", columnDefinition = "TEXT[]")
//    private List<String> tags;

  @CreationTimestamp
  @Column(name = "created_at")
  private Timestamp createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at")
  private Timestamp updatedAt;

}
