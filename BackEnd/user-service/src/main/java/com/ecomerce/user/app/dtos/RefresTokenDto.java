package com.ecomerce.user.app.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RefresTokenDto {
    @NotNull
    private String refreshToken;

    private String service;
}
