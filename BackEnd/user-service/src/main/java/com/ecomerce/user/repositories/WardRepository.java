package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.views.Ward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WardRepository extends JpaRepository<Ward,Long> {
    List<Ward> findByDistrictId(long dId);
}
