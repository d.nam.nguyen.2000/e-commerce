package com.ecomerce.user.app.dtos;

import lombok.Data;

@Data
public class ChangeStatePurchasesDto {
  private String status;
}
