package com.ecomerce.user.entities;

import com.ecomerce.user.entities.views.Product;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "carts")
public class Cart {
  @ManyToOne()
  @JoinColumn(name = "product_id", insertable = false, updatable = false)
  @NotFound(action = NotFoundAction.IGNORE)
  Product product;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @JsonIgnore
  @Column(name = "product_id")
  private Long productId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", insertable = false, updatable = false)
  @NotFound(action = NotFoundAction.IGNORE)
  @JsonIgnoreProperties({"purchases", "cart", "reviews"})
  @ToString.Exclude
  User user;

  @JsonIgnore
  @Column(name = "user_id")
  private Long userId;

  @Column(name = "quantity")
  private Integer quantity;

  @CreationTimestamp
  @Column(name = "created_at")
  private Timestamp createdAt;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Cart cart = (Cart) o;
    return id != null && Objects.equals(id, cart.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
