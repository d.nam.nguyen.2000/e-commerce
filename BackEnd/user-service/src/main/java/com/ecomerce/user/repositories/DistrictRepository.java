package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.views.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRepository extends JpaRepository<District,Long> {
    List<District> findByProvinceId(long pId);
}
