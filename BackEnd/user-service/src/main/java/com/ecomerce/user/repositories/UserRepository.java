package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

//  @Query("SELECT p FROM user p "
//          + " WHERE ((:keyword IS NULL OR LOWER(p.name) LIKE CONCAT('%', LOWER(:keyword), '%'))"
//          + " OR (:keyword IS NULL OR LOWER(p.email) LIKE CONCAT('%', LOWER(:keyword), '%'))"
//          + " OR (:keyword IS NULL OR LOWER(p.phone) LIKE CONCAT('%', LOWER(:keyword), '%')))"
//          + " ORDER BY p.createdAt DESC")
//  Page<User> filter(
//          String keyword,
//          Pageable pageable);

  boolean existsByPhone(String phone);
  boolean existsByEmail(String email);

  @Query("select u from User u where u.email = :email or u.phone = :phone")
  Optional<User> findByEmailOrName(String email, String phone);

  List<User> findAllByStatusAndRole(String status, String role);
}
