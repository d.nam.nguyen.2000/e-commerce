package com.ecomerce.user.service;

import com.ecomerce.user.app.dtos.FavoriteDto;
import com.ecomerce.user.entities.Favorite;
import com.ecomerce.user.entities.views.Product;
import com.ecomerce.user.repositories.FavoriteRepository;
import com.ecomerce.user.utils.FnCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class FavoriteService extends BaseService {

  @Autowired private ProductService productService;
  @Autowired private FavoriteRepository favoriteRepository;

  public List<Favorite> addMutiToFavorite(Long userId, List<FavoriteDto> dtoes) {
    List<Favorite> favorites = new ArrayList<>();
    for (FavoriteDto dto : dtoes) {
      Favorite favorite = new Favorite();
      favorite.setUserId(userId);
      favorite.setProductId(dto.getProductId());
      favorites.add(favorite);
    }
    return favoriteRepository.saveAll(favorites);
  }

  public Favorite addToFavorite(Long userId, FavoriteDto dtoes) {
    Favorite favorite = new Favorite();
    favorite.setUserId(userId);
    favorite.setProductId(dtoes.getProductId());
    return favoriteRepository.save(favorite);
  }

  public List<Favorite> updateFavorite(Long userId, List<FavoriteDto> dto) {
    List<Favorite> Favorites = new ArrayList<>();
    //    for (FavoriteDto FavoriteDto : dto) {
    //      ProductFavoriteSearch check =
    //          (ProductFavoriteSearch)
    //              FnCommon.convertToEntity(
    //                  productRepository.getByVariant(FavoriteDto.getVariantId()),
    // ProductFavoriteSearch.class);
    //      Favorite Favorite = favoriteRepository.findByUserIdAndVariantId(userId,
    // FavoriteDto.getVariantId());
    //      if (Favorite == null) {
    //        throw new AppException(HttpStatus.BAD_REQUEST,
    // MessageUtils.getMsg("Favorite_not_found"));
    //      }
    //      if ((check.getQuantity() - check.getSolQuantity()) < (FavoriteDto.getQuantity())) {
    //        throw new AppException(HttpStatus.BAD_REQUEST,
    // MessageUtils.getMsg("Favorite_max_quantity"));
    //      }
    //      Favorite.setQuantity(FavoriteDto.getQuantity());
    //
    //      favoriteRepository.save(Favorite);
    //      Favorites.add(Favorite);
    //    }
    return Favorites;
  }

  @Transactional
  public void deleteFavorite(Long id, Long userId) {
    favoriteRepository.deleteById(id);
  }

  public List<Favorite> getFavorite(String role, Long userId) {
    if (role.equals("admin")) {
      return favoriteRepository.findAll();
    }
    return favoriteRepository.findByUserId(userId);
  }
}
