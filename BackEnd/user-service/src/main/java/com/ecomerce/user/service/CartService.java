package com.ecomerce.user.service;

import com.ecomerce.user.app.dtos.CartDto;
import com.ecomerce.user.entities.Cart;
import com.ecomerce.user.utils.FnCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartService extends BaseService {

  @Autowired private ProductService productService;

  public Cart addToCart(Long userId, CartDto dto) {
    Cart cart = FnCommon.copyNonNullProperties(Cart.class, dto);
    cart.setUserId(userId);
    return cartRepository.save(cart);
  }

  public List<Cart> updateCart(Long userId, List<CartDto> dto) {
    List<Cart> carts = new ArrayList<>();
    //    for (CartDto cartDto : dto) {
    //      ProductCartSearch check =
    //          (ProductCartSearch)
    //              FnCommon.convertToEntity(
    //                  productRepository.getByVariant(cartDto.getVariantId()),
    // ProductCartSearch.class);
    //      Cart cart = cartRepository.findByUserIdAndVariantId(userId, cartDto.getVariantId());
    //      if (cart == null) {
    //        throw new AppException(HttpStatus.BAD_REQUEST, MessageUtils.getMsg("cart_not_found"));
    //      }
    //      if ((check.getQuantity() - check.getSolQuantity()) < (cartDto.getQuantity())) {
    //        throw new AppException(HttpStatus.BAD_REQUEST,
    // MessageUtils.getMsg("cart_max_quantity"));
    //      }
    //      cart.setQuantity(cartDto.getQuantity());
    //
    //      cartRepository.save(cart);
    //      carts.add(cart);
    //    }
    return carts;
  }

  @Transactional
  public void deleteCart(Long id, Long userId) {
    cartRepository.deleteById(id);
  }

  public List<Cart> getCart(Long userId, String role) {
   if (role.equals("admin")) {
     return cartRepository.findAll();
   }
    return cartRepository.findByUserId(userId);
  }
}
