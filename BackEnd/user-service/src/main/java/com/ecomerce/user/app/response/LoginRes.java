package com.ecomerce.user.app.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginRes {
  private String token;
  private String refreshToken;
  private Map<String,Object> claims;

  public LoginRes(String token, String refreshToken, Map<String,Object> claims){
    this.token = token;
    this.refreshToken = refreshToken;
    this.claims = claims;
  }

  public LoginRes(String token, Map<String,Object> claims){
    this.token = token;
    this.claims = claims;
  }
}
