package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.views.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
  @Query(
      value =
          "select v.id as variant_id, v.product_id, p.shop_id, " +
                  "s.user_id as shop_user_id, v.sale_price, v.quantity," +
                  " v.sol_quantity, CONCAT('p.transport_fee', '::', 'jsonb')   as transport_fee from products.product_variant v " +
            "join products.product p on p.id = v.product_id " +
            "join stores.shop s on p.shop_id = s.id " +
            "where v.id = :vId",
      nativeQuery = true)
  Tuple getByVariant(long vId);
}
