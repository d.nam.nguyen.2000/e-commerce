package com.ecomerce.user.app.controller;

import com.ecomerce.user.app.dtos.FavoriteDto;
import com.ecomerce.user.entities.Favorite;
import com.ecomerce.user.service.FavoriteService;
import com.ecomerce.user.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("user/favorite")
public class FavoriteController {
  @Autowired FavoriteService favoriteService;

  @GetMapping()
  public ResponseEntity<List<Favorite>> getFavorite(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @RequestHeader(name = Constant.headerRole, required = false) String role) {

    return ResponseEntity.ok(favoriteService.getFavorite(role, userId));
  }

  @PostMapping("/muti")
  public ResponseEntity<List<Favorite>> addMultiToFavorite(
      @RequestHeader(name = Constant.headerUserId) Long userId, @RequestBody @Valid List<FavoriteDto> dto) {
    return ResponseEntity.ok(favoriteService.addMutiToFavorite(userId, dto));
  }

  @PostMapping()
  public ResponseEntity<Favorite> addToFavorite(
          @RequestHeader(name = Constant.headerUserId) Long userId, @RequestBody @Valid FavoriteDto dto) {
    return ResponseEntity.ok(favoriteService.addToFavorite(userId, dto));
  }

  @PutMapping()
  public ResponseEntity<List<Favorite>> updateFavorite(
      @RequestHeader(name = Constant.headerUserId) Long userId, @RequestBody @Valid List<FavoriteDto> dto) {
    return ResponseEntity.ok(favoriteService.updateFavorite(userId, dto));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteFavorite(
          @PathVariable Long id,
      @RequestHeader(name = Constant.headerUserId) Long userId) {
    favoriteService.deleteFavorite(id, userId);
    return ResponseEntity.ok().build();
  }
}
