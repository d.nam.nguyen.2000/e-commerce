package com.ecomerce.user.entities.enums;

public enum Gender {
  MALE,
  FEMALE
}
