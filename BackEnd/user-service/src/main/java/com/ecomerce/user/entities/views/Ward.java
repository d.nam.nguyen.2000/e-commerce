package com.ecomerce.user.entities.views;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "m_ward", schema = "public")
public class Ward {
  @Id private Long id;

  @Column(name = "district_id")
  private Long districtId;

  private String name;
}
