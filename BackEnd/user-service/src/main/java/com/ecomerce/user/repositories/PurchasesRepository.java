package com.ecomerce.user.repositories;

import com.ecomerce.user.entities.Favorite;
import com.ecomerce.user.entities.Purchases;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchasesRepository extends JpaRepository<Purchases, Long> {

  List<Purchases> findByCustomerId(long uId);
  List<Purchases> findAllByStoreId(long storeId);

}
