package com.ecomerce.user.utils;

public class Constant {
  public static final String headerUserId = "user-id";
  public static final String headerStoreId = "shop-id";
  public static final String headerRole = "role";
  public static final String headerStatus = "status";
}
