package com.ecomerce.user.service;

import com.ecomerce.user.app.dtos.StoreChangeStateDto;
import com.ecomerce.user.app.dtos.UserDto;
import com.ecomerce.user.entities.User;
import com.ecomerce.user.exceptions.AppException;
import com.ecomerce.user.repositories.UserRepository;
import com.ecomerce.user.utils.FnCommon;
import com.ecomerce.user.utils.MessageUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
  @Autowired ModelMapper modelMapper;
  @Autowired
  UserRepository userRepository;

  public User updateUser(Long userId, UserDto dto) {
      User user = userRepository.findById(userId).orElse(new User());
    user.setId(userId);
    FnCommon.coppyNonNullProperties(user, dto);

    return userRepository.save(user);
  }

  public List<User> getListUser(Optional<String> keyword, Pageable pageable) {
    return userRepository.findAll();
  }

  public User getUserById(Long userId) {
    return userRepository.findById(userId).orElseThrow(() -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("user_not_found")));
  }

  public User getUserByEmail(String email) {
    return userRepository.findByEmailOrName(email, email).orElseThrow(() -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("user_not_found")));
  }

  @Transactional
  public User registerSeller(Long userId, StoreChangeStateDto dto) {
    User user = userRepository.findById(userId).orElse(new User());
    user.setId(userId);
    user.setStatus(dto.getStatus());
    user.setRole(dto.getRole());
    return userRepository.save(user);
  }

  public List<User> getRegisterSeller() {
    return userRepository.findAllByStatusAndRole("inactive", "seller");
  }
}
