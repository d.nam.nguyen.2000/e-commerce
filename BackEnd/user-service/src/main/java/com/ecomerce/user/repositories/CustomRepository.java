package com.ecomerce.user.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;

@Repository
public interface CustomRepository {
  @Query(
      value =
          "select * from public.ward w inner public.district d on d.id = w.district_id inner join public.province p on p.id = d.province_id where w.id=:wId",
      nativeQuery = true)
  Tuple getFullWardRelationById(int wId);
}
