package com.ecomerce.user.app.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
  @NotBlank(message = "Name is required")
  @Size(max = 100, message = "Name should be at most 100 characters")
  private String name;

  @NotBlank(message = "Email is required")
  @Pattern(regexp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "Please provide a valid email address")
  private String email;

  @NotBlank(message = "Password is required")
  @Size(min = 8, max = 20, message = "Password should be between 8 and 20 characters")
  private String password;

  @NotBlank(message = "Phone number is required")
  @Pattern(regexp = "\\+?[0-9]+", message = "Please provide a valid phone number")
  private String phone;

  @NotBlank(message = "Role is required")
  private String role;

  @NotBlank(message = "Status is required")
  private String status;

  @NotNull(message = "Avatar is required")
  private String avatarUrl;

  private String address;
}
