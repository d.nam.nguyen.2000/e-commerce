package com.ecomerce.user.app.dtos;

import lombok.Data;

import java.util.Date;
import java.util.Map;

@Data
public class GetTokenDto {
  private Date birthDay;

  private String name;

  private String email;

  private String avatar;

  private String username;

  private String token;

  private Map<String, Object> metadata;
}
