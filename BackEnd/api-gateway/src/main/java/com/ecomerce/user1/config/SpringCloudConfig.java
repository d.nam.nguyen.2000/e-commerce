package com.ecomerce.user1.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder, TokenDecodeGatewayFilterFactory filterFactory) {
        return builder.routes()
                .route("order-service", r -> r.path("/e-order/**")
                        .filters(f -> f.filter(filterFactory.apply(new TokenDecodeGatewayFilterFactory.Config())))
                        .uri("lb://ORDER-SERVICE"))

                .route("product-service", r -> r.path("/e-product/**")
                        .filters(f -> f.filter(filterFactory.apply(new TokenDecodeGatewayFilterFactory.Config())))
                        .uri("lb://PRODUCT-SERVICE"))

                .route("user-service", r -> r.path("/e-user/**")
                        .filters(f -> f.filter(filterFactory.apply(new TokenDecodeGatewayFilterFactory.Config())))
                        .uri("lb://USER-SERVICE"))

                .route("shop-service", r -> r.path("/e-shop/**")
                        .filters(f -> f.filter(filterFactory.apply(new TokenDecodeGatewayFilterFactory.Config())))
                        .uri("lb://SHOP-SERVICE"))
                .build();
    }

}

