package com.ecomerce.shop.app.controller;

import io.imagekit.sdk.ImageKit;
import io.imagekit.sdk.config.Configuration;
import io.imagekit.sdk.models.FileCreateRequest;
import io.imagekit.sdk.models.results.Result;
import io.imagekit.sdk.utils.Utils;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/images")
public class ImageController {

    private final ImageKit imageKit;

    public ImageController() {
        Configuration config = new Configuration(
                "public_QFThLW1EwqQDV5A4GsURnw7zhbU=",
                "private_8z29JewninndEuIvGP9ggsRnZXU=",
                "https://ik.imagekit.io/ecommerk13haui");

        this.imageKit = ImageKit.getInstance();
        this.imageKit.setConfig(config);
    }

    @SneakyThrows
    @PostMapping("/upload")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) {
        try {
            byte[] fileData = file.getBytes();
            String base64 = Utils.bytesToBase64(fileData);
            FileCreateRequest fileCreateRequest = new FileCreateRequest(base64, "file_name.jpg");
            Result result = imageKit.upload(fileCreateRequest); // Sử dụng imageKit đã được inject

            // Trả về URL của ảnh đã upload
            return ResponseEntity.ok(result.getUrl());
        } catch (IOException e) {
            return ResponseEntity.ok("https://placehold.co/296x200.png");
        }
    }

    @SneakyThrows
    @PostMapping("/muti_upload")
    public ResponseEntity<List<String>> uploadImages(@RequestParam("files") List<MultipartFile> files) {
        List<String> uploadedUrls = new ArrayList<>();

        for (MultipartFile file : files) {
            try {
                byte[] fileData = file.getBytes();
                String base64 = Utils.bytesToBase64(fileData);
                FileCreateRequest fileCreateRequest = new FileCreateRequest(base64, "file_name.jpg");
                Result result = imageKit.upload(fileCreateRequest); // Sử dụng imageKit đã được inject

                // Thêm URL của ảnh đã upload vào danh sách
                uploadedUrls.add(result.getUrl());
            } catch (IOException e) {
                // Nếu có lỗi xảy ra, thêm URL mặc định vào danh sách
                uploadedUrls.add("https://placehold.co/296x200.png");
            }
        }

        // Trả về danh sách các URL của ảnh đã upload
        return ResponseEntity.ok(uploadedUrls);
    }
}

