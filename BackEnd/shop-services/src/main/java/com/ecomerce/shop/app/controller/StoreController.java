package com.ecomerce.shop.app.controller;

import com.ecomerce.shop.app.dtos.StoreDto;
import com.ecomerce.shop.app.dtos.UpdateStoreDto;
import com.ecomerce.shop.app.response.ResponsePage;
import com.ecomerce.shop.domain.entities.Store;
import com.ecomerce.shop.domain.entities.enums.ShopState;
import com.ecomerce.shop.domain.entities.enums.ShopType;
import com.ecomerce.shop.domain.services.ShopService;
import com.ecomerce.shop.domain.utils.Constant;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("store")
public class StoreController {
  @Autowired ShopService shopService;
  
  @PutMapping()
  public ResponseEntity<Store> updateStore(
      @RequestHeader(name = Constant.headerUserId) Long userId,
      @RequestHeader(name = Constant.headerShopId) Long shopId,
      @RequestBody @Valid StoreDto dto) {
    return ResponseEntity.ok(shopService.updateStore(userId, shopId, dto));
  }

  @PutMapping("/{shopId}")
  public ResponseEntity<Store> updateStoreById(
          @RequestHeader(name = Constant.headerUserId) Long userId,
          @PathVariable Long shopId,
          @RequestBody StoreDto dto) {
    return ResponseEntity.ok(shopService.updateStore(userId, shopId, dto));
  }
  /**
   * Chi tiết shop
   *
   * @param shopId
   * @return
   */
  @GetMapping("/{shopId}")
  public ResponseEntity<Store> detailStore(@PathVariable Long shopId) {
    return ResponseEntity.ok(shopService.detailStore(shopId));
  }

  @GetMapping("/me")
  public ResponseEntity<Store> detailStoreMe(@RequestHeader(name = Constant.headerUserId) Long userId) {
    return ResponseEntity.ok(shopService.detailStoreMe(userId));
  }

  @GetMapping()
  public ResponseEntity<List<Store>> getListShop(
          @RequestParam(required = false) Optional<String> phone,
          @RequestParam(required = false) Optional<String> name,
          @RequestParam(required = false) Optional<String> contactName,
          @RequestParam(required = false) Optional<String> email,
          @RequestParam(required = false) Optional<String> local,
          @RequestParam(required = false) Double star,
          @RequestParam(required = false) ShopState state,
          @RequestParam(required = false) ShopType type) {
    return ResponseEntity.ok(shopService.getListShop(
            phone, name, contactName, email, local, star, state, type));
  }


  /**
   * Đăng ký shop
   *
   * @param userId
   * @param dto
   * @return
   */
  @PostMapping()
  public ResponseEntity<Store> registerStore(
      @RequestHeader(name = Constant.headerUserId) Long userId,
      @Valid @RequestBody StoreDto dto) {
    return ResponseEntity.ok(shopService.createStore(userId, dto));
  }

//  /**
//   * duyệt shop
//   *
//   * @param shopId
//   * @return
//   */
//  @PostMapping("/{shopId}/approve")
//  public ResponseEntity approveStore(
//      @PathVariable Long shopId, @RequestHeader(name = Constant.headerIsAdmin) Boolean isAdmin) {
//    return ResponseEntity.ok(shopService.approveStore(isAdmin, shopId));
//  }
//
//  /**
//   * duyệt shop
//   *
//   * @param shopId
//   * @return
//   */
//  @PostMapping("/{shopId}/state")
//  public ResponseEntity changeState(
//      @PathVariable Long shopId,
//      @RequestHeader(name = Constant.headerIsAdmin) Boolean isAdmin,
//      @RequestBody @Valid ChangeStateDto dto) {
//    return ResponseEntity.ok(shopService.changeState(isAdmin, shopId, dto));
//  }
//
//  /**
//   * get dashboard
//   *
//   * @param shopId
//   * @return
//   */
//  @GetMapping("/dashboard")
//  public ResponseEntity<Dashboard> getDashboard(
//      @RequestHeader(name = Constant.headerShopId) Long shopId) {
//    return ResponseEntity.ok(shopService.getDashboard(shopId));
//  }
}
