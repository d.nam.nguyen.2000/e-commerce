package com.ecomerce.shop;

import io.imagekit.sdk.ImageKit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import io.imagekit.sdk.config.Configuration;

@SpringBootApplication
@EnableEurekaClient
public class ShopServiceApplication {

    public static void main(String[] args) {

        ImageKit imageKit = ImageKit.getInstance();
        Configuration config = new Configuration(
                "public_QFThLW1EwqQDV5A4GsURnw7zhbU=",
                "private_8z29JewninndEuIvGP9ggsRnZXU=",
                "https://ik.imagekit.io/ecommerk13haui");
        imageKit.setConfig(config);

        SpringApplication.run(ShopServiceApplication.class, args);

    }

}
