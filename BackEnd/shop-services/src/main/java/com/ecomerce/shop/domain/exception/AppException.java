package com.ecomerce.shop.domain.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class AppException extends RuntimeException {
  HttpStatus status;

  public AppException(HttpStatus status, String msg) {
    super(msg);
    this.status = status;
  }
}
