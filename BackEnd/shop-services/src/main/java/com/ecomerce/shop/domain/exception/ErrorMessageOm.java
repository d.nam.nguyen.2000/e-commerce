package com.ecomerce.shop.domain.exception;

public enum ErrorMessageOm {
  ACCOUNT_NOT_PERMISSION("Tài khoản không có quyền"),
  PHONE_HAS_BEEN_SHOP("Số điện thoại đã được tạo shop khác"),
  ACCOUNT_HAS_BEEN_SHOP("Tài khoản đã có shop"),
  ACCOUNT_HAS_BEEN_REGISTER_SHOP("Tài khoản đã được dùng tạo shop. Hãy đợi quản trị duyệt shop"),
  CAN_NOT_CHANGE_STATE("Không thể chuyển trạng thái"),
  CAN_NOT_DELETE_PRO("Không thể xóa sản phẩm"),
  SHOP_ID_NOT_NULL("shopId not null"),
  USER_NOT_FOUND("Không tìm thấy user"),
  SHOP_NOT_FOUND("Không tìm thấy shop"),
  SHOP_HAS_BEEN_BLOCK("Shop đã bị khóa"),
  SHOP_ADDRESS_IS_DEFAULT("Không thể xóa địa chỉ mặc định"),
  PRODUCT_NOT_FOUND("Không tìm thấy sản phẩm");

  public String val;

  private ErrorMessageOm(String label) {
    val = label;
  }
}
