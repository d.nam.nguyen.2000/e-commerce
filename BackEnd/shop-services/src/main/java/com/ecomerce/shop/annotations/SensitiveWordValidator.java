package com.ecomerce.shop.annotations;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Component
public class SensitiveWordValidator implements ConstraintValidator<SensitiveWord, String> {

    @Override
    public void initialize(SensitiveWord constraintAnnotation) {
        // initialize annotation do something
    }

    @SneakyThrows
    @Override
    public boolean isValid(String text, ConstraintValidatorContext context) {
        if (text == null || text.isEmpty()) return false;
        return isSensitiveWord(text);
    }

    private boolean isSensitiveWord(String text) throws IOException {
        text = text.trim().toLowerCase().replace(" ", "_");
        List<String> ListSensitive = Files.readAllLines(Paths.get("/bad_words.txt"));
        for (String line : ListSensitive) {
            if (line.hashCode() != 0 && text.contains(line.trim()) ){
                return false;
            }
        }
        return true;
    }
}
