package com.ecomerce.shop.domain.entities.enums;

public enum ShopType {
  PERSON,
  SHOP,
  ENTERPRISES,
  FACILITY,
  COOPERATIVE,
  POST_OFFICES
}
