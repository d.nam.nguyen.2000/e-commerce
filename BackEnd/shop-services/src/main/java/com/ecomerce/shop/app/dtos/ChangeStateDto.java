package com.ecomerce.shop.app.dtos;

import javax.validation.constraints.NotNull;

import com.ecomerce.shop.domain.entities.enums.ShopState;
import lombok.Data;

@Data
public class ChangeStateDto {
  @NotNull private ShopState state;
}
