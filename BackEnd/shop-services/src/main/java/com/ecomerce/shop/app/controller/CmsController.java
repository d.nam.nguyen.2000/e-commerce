//package com.ecomerce.shop.app.controller;
//
//import com.ecomerce.shop.app.dtos.CreateStoreDto;
//import com.ecomerce.shop.app.dtos.UpdateStoreDto;
//import com.ecomerce.shop.domain.services.ShopService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@CrossOrigin
//@RestController
//@RequestMapping("v1/cms")
//public class CmsController {
//  @Autowired
//  ShopService shopService;
//
//  @PostMapping("shops")
//  public ResponseEntity<Store> createStoreCms(
//          @RequestBody @Valid CreateStoreDto dto) {
//    return ResponseEntity.ok(shopService.createStore(dto));
//  }
//
//  @PutMapping("shops/{shopId}")
//  public ResponseEntity<Store> updateStoreCms(
//          @RequestBody @Valid UpdateStoreDto dto, @PathVariable Long shopId) {
//    return ResponseEntity.ok(shopService.updateStoreCms(shopId, dto));
//  }
//}
