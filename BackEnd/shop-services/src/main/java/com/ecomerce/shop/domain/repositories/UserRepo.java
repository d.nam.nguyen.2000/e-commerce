package com.ecomerce.shop.domain.repositories;

import com.ecomerce.shop.domain.entities.views.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
}
