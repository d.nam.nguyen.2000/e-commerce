package com.ecomerce.shop.domain.entities.views;

import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@ToString
@Table(name = "categories")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
@TypeDef(name = "string-array", typeClass = StringArrayType.class)
//@Where(clause = "state <> 'DELETED'")
public class Category {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "title", nullable = false)
  private String title;

  @Column(name = "thumbnail_url")
  private String thumbnailUrl;

  @Column(name = "description")
  private String description;

  @Type(type = "string-array" )
  @Column(name = "keynotes", nullable = false)
  private String[] keynotes;

  @Type(type = "string-array" )
  @Column(name = "tags")
  private String[] tags;

  @Column(name = "creator_id")
  private Long creatorId;

  @ManyToOne
  @JoinColumn(name = "creator_id", insertable = false, updatable = false)
  @NotFound(action = NotFoundAction.IGNORE)
  private User creator;

  @CreationTimestamp
  @Column(name = "created_at")
  private Date createdAt;

  @UpdateTimestamp
  @Column(name = "updated_at")
  private Date updatedAt;
}
