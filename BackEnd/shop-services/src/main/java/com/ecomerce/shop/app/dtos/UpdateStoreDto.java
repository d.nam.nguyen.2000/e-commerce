package com.ecomerce.shop.app.dtos;

import com.ecomerce.shop.domain.entities.enums.ShopType;
import lombok.Data;

@Data
public class UpdateStoreDto {
    private String name;
    private Long userId;
    private ShopType shopType;
    private String description;
    private String avatar;
    private String banner;
    private String website;
    private Integer provinceId;
    private Integer districtId;
    private Integer wardId;
    private String identityCard;
    private String taxNumber;
    private String phone;
    private String email;
    private String address;
}
