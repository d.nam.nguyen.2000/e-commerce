package com.ecomerce.shop.domain.repositories;

import com.ecomerce.shop.domain.entities.Store;
import com.ecomerce.shop.domain.entities.enums.ShopState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StoreRepo extends JpaRepository<Store, Long> {
//    Page<Store> findAllByAndState(ShopState state, Pageable pageable);

//  @Query(value = "SELECT p.* FROM stores.Shop p  left join stores.shop_stats ss on p.id = ss.shop_id"
//          + " WHERE (:name IS NULL OR LOWER(p.name) LIKE CONCAT('%', LOWER(:name), '%'))"
//          + " AND (:email IS NULL OR LOWER(p.email) LIKE CONCAT('%', LOWER(:email), '%'))"
//          + " AND (:phone IS NULL OR LOWER(p.phone) LIKE CONCAT('%', LOWER(:phone), '%'))"
//          + " AND (:contactName IS NULL OR LOWER(p.contact_name) LIKE CONCAT('%', LOWER(:contactName), '%'))"
//          + " AND (:local IS NULL OR (LOWER(p.province ->> 'name') LIKE CONCAT('%', LOWER(:local), '%')"
//          + "    OR LOWER(p.ward ->> 'name') LIKE CONCAT('%', LOWER(:local), '%')"
//          + "    OR LOWER(p.district ->> 'name') LIKE CONCAT('%', LOWER(:local), '%')))"
//          + " AND (:star IS NULL OR ss.aggregate >= :star)"
//          + " AND (:state IS NULL OR cast(p.state as text) = :state)"
//          + " AND (:type IS NULL OR cast(p.type as text) = :type)"
//          + " ORDER BY p.created_at DESC", nativeQuery = true)
//  Page<Store> filter(
//      String phone,
//      String name,
//      String contactName,
//      String email,
//      String local,
//      Double star,
//      String state,
//      String type,
//      Pageable pageable);

    Optional<Store> findByOwnerId(Long userId);
}
