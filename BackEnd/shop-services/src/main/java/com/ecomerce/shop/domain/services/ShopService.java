package com.ecomerce.shop.domain.services;

import com.ecomerce.shop.app.dtos.*;
import com.ecomerce.shop.domain.entities.Store;
import com.ecomerce.shop.domain.entities.enums.ShopState;
import com.ecomerce.shop.domain.entities.enums.ShopType;
import com.ecomerce.shop.domain.repositories.*;
import com.ecomerce.shop.domain.utils.FnCommon;
import com.ecomerce.shop.domain.utils.MessageUtils;
import com.ecomerce.shop.domain.entities.views.User;
import com.ecomerce.shop.domain.exception.AppException;
import com.ecomerce.shop.domain.exception.ErrorMessageOm;
import com.ecomerce.shop.domain.exception.ExceptionOm;

import com.google.gson.Gson;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ShopService {
    @Autowired
    StoreRepo storeRepo;
    @Autowired
    UserRepo userRepo;
    ModelMapper modelMapper = new ModelMapper();

    @Transactional
    public Store createStore(Long userId, StoreDto dto) {
        Store shop = new Store();
        FnCommon.coppyProperties(shop, dto);
        if (!dto.getKeynotes().isEmpty())
            shop.setKeynotes(convertArrStr(dto.getKeynotes()));
        if (!dto.getTags().isEmpty())
            shop.setTags(convertArrStr(dto.getTags()));
        shop.setStatus("active");
        shop.setOwnerId(userId);
        storeRepo.save(shop);

        return shop;
    }

    public Store updateStore(Long userId, Long shopId, StoreDto dto) {
        Store shop = storeRepo
                .findById(shopId)
                .orElseThrow(
                        () ->
                                new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("shop_not_found")));
        if (!dto.getKeynotes().isEmpty())
            shop.setKeynotes(convertArrStr(dto.getKeynotes()));
        if (!dto.getTags().isEmpty())
            shop.setTags(convertArrStr(dto.getTags()));
        FnCommon.coppyNonNullProperties(shop, dto);

        return storeRepo.save(shop);
    }


    public Store detailStore(Long shopId) {
        return storeRepo
                .findById(shopId)
                .orElseThrow(
                        () -> new AppException(HttpStatus.NOT_FOUND, MessageUtils.getMsg("shop_not_found")));
    }

    public Store detailStoreMe(Long userId) {
        return storeRepo
                .findByOwnerId(userId)
                .orElse(null);
    }


    public List<Store> getListShop(
            Optional<String> phone,
            Optional<String> name,
            Optional<String> contactName,
            Optional<String> email,
            Optional<String> local,
            Double star,
            ShopState state,
            ShopType type) {
//    return storeRepo.filter(
//        phone.map(String::toLowerCase).orElse(null),
//        name.map(String::toLowerCase).orElse(null),
//        contactName.map(String::toLowerCase).orElse(null),
//        email.map(String::toLowerCase).orElse(null),
//        local.map(String::toLowerCase).orElse(null),
//        star,
//        state == null ? null : state.toString(),
//        type == null ? null : type.toString(),
//        pageable);
        return storeRepo.findAll();
    }


    //  public ResponseEntity<String> changeState(Boolean isAdmin, Long shopId, ChangeStateDto dto) {
//    if (!isAdmin)
//      throw new AppException(HttpStatus.BAD_REQUEST, ErrorMessageOm.ACCOUNT_NOT_PERMISSION.val);
//    Store shop = detailStore(shopId);
//    shop.setState(dto.getState());
//    storeRepo.save(shop);
//    // send mail
//    // TODO
//    return ResponseEntity.ok().build();
//  }
    public static String[] convertArrStr(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, String[].class);
    }
}
