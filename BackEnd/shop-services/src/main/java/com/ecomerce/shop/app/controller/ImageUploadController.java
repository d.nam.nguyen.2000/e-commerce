package com.ecomerce.shop.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

@RestController
@RequestMapping("/file")
public class ImageUploadController {

    @Autowired
    private ServletContext servletContext;

    private final String uploadDir = "./src/main/resources/images/";
    private final String domain = "http://localhost:8080/e-shop/file/read/";

    @PostMapping("/upload")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) {
        try {
            // Lấy đường dẫn để lưu trữ tập tin tải lên
            String uploadDir = "./src/main/resources/images/";

            // Kiểm tra xem thư mục có tồn tại không, nếu không, tạo mới
            File directory = new File(uploadDir);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            // Tạo tên tập tin duy nhất dựa trên thời gian
            String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();

            // Tạo đối tượng File từ đường dẫn và tên tập tin
            File uploadFile = new File(uploadDir + fileName);

            // Tạo luồng đầu ra để ghi dữ liệu từ tập tin tải lên vào tập tin mới
            FileOutputStream fos = new FileOutputStream(uploadFile);
            fos.write(file.getBytes());
            fos.close();

            // Trả về đường dẫn của tập tin đã tải lên
            return new ResponseEntity<>(domain + fileName, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>("Failed to upload file", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //    @GetMapping("/read/{imageName}")
//    public ResponseEntity<String> readImage(@PathVariable String imageName) {
//        try {
//            // Đọc ảnh từ thư mục lưu trữ
//            Path imagePath = Paths.get(uploadDir1 + File.separator + imageName);
//            byte[] imageBytes = Files.readAllBytes(imagePath);
//            // Encode ảnh dưới dạng base64 để trả về dưới dạng chuỗi
//            String base64EncodedImage = Base64.getEncoder().encodeToString(imageBytes);
//            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(base64EncodedImage);
//        } catch (IOException e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to read image");
//        }
//    }
    @GetMapping("/read/{imageName}")
    public ResponseEntity<byte[]> readImage(@PathVariable String imageName) {
        try {
            // Đọc ảnh từ thư mục lưu trữ
            Path imagePath = Paths.get(uploadDir + File.separator + imageName);
            byte[] imageBytes = Files.readAllBytes(imagePath);
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(imageBytes);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
