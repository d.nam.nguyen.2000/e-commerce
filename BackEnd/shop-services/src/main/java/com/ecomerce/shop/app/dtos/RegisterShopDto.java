package com.ecomerce.shop.app.dtos;

import com.ecomerce.shop.annotations.Email;
import com.ecomerce.shop.domain.entities.enums.ShopType;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class RegisterShopDto {
  @NotBlank
  private String contactName;
  @NotBlank
//  @SensitiveWord
  @Max(value = 255, message = "Tên shop quá dài")
  private String name;
  @NotBlank
  private String phone;
  @NotBlank
  @Email
  private String email;
  @NotBlank
  private String identityCard;
  @NotBlank
  private String taxNumber;

  private String refCode;
  @NotNull
  private Long provinceId;
  @NotNull
  private Long districtId;
  @NotNull
  private Long wardId;

  private String Address;

  @NotNull
  private ShopType shopType;


//  private String vnp_pay_username;
//  private String vnp_pay_id;

//  @NotBlank
//  @Size(min = 1, max = 2, message = "is_checkbox not valid")
//  private int is_checkbox;
//
//  @AssertTrue(message = "Thông tin ngân hàng không được để trống")
//  protected boolean checkBank(){
//    if (is_checkbox == 1){
//      if (bank_name == null || bank_branch == null || bank_number == null || account_holder == null)
//        return true;
//      else
//        return false;
//    } else {
//      if (vnp_pay_username == null || vnp_pay_id == null)
//        return true;
//      else
//        return false;
//    }
//  }
}
