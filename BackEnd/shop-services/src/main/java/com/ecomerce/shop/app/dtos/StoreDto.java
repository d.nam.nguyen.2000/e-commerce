package com.ecomerce.shop.app.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreDto {
    @NotBlank(message = "Title is required")
    @Size(max = 100, message = "Title should be at most 100 characters")
    private String title;

    @NotBlank(message = "Description is required")
    private String description;

    @NotNull(message = "Thumbnail is required")
    private String thumbnailUrl;

    private String keynotes;

    private String address;

    private String tags;

}
