package com.ecomerce.shop.app.dtos;

import com.ecomerce.shop.domain.entities.enums.ShopType;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CreateStoreDto {
  @NotEmpty(message = "Tên shop không được để trống")
  @Max(value = 255, message = "Tên shop quá dài")
  private String name;
  @NotNull(message = "Id user không được để trống")
  private Long userId;
  private String description;
  @NotEmpty(message = "Ảnh đại diện shop không được trống")
  @Max(value = 255, message = "Đường dẫn ảnh quá dài")
  private String avatar;
  @NotEmpty(message = "Ảnh banner shop không được trống")
  @Max(value = 255, message = "Đường dẫn ảnh quá dài")
  private String banner;
  private String website;
  @NotNull(message = "Tỉnh/tp không được để trống")
  private Long provinceId;
  @NotNull(message = "Quận/Huyện không được để trống")
  private Long districtId;
  @NotNull(message = "Phường/Xã không được để trống")
  private Long wardId;
  private String address;
  private String phone;
  @NotBlank
  private String identityCard;
  @NotBlank
  private String taxNumber;
  @NotNull
  private ShopType shopType;
}
