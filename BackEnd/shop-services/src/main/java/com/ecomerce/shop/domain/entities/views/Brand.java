package com.ecomerce.shop.domain.entities.views;

import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "brands")
@TypeDef(name = "string-array", typeClass = StringArrayType.class)
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, length = 100)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "logo_url", length = 255)
    private String logoUrl = "https://placehold.co/296x200.png";

    @Column(name = "creator_id")
    private Long creatorId;

    @ManyToOne
    @JoinColumn(name = "creator_id", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private User creator;

    @Type(type = "string-array" )
    @Column(name = "keynotes")
    private String[] keynotes;

    @Type(type = "string-array" )
    @Column(name = "tags")
    private String[] tags;

    @CreationTimestamp
    @Column(name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private Date updatedAt;

}
