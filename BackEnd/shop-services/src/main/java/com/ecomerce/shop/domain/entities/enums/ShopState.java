package com.ecomerce.shop.domain.entities.enums;

public enum ShopState {
  ACTIVE,
  INACTIVE,
  PENDING,
  DELETED,
  BLOCK,
  REJECT
}
