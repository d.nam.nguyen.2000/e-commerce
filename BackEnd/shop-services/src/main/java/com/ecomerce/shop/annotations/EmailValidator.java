package com.ecomerce.shop.annotations;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

@Component
public class EmailValidator implements ConstraintValidator<Email, String> {

    @Override
    public void initialize(Email constraintAnnotation) {
        // initialize annotation do something
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        if (email == null || email.isEmpty()) return true;
        return isIncorrectUsername(email);
    }

    private boolean isIncorrectUsername(String email) {
        Pattern pattern = Pattern.compile
                ("^[A-Za-z0-9\\.][A-Za-z0-9\\.][A-Za-z0-9\\.][A-Za-z0-9\\.][A-Za-z0-9\\.][A-Za-z0-9\\.]{0,62}[A-Za-z0-9]@[A-Za-z0-9][A-Za-z0-9\\-\\.]{0,251}[A-Za-z0-9]$");
        if (pattern.matcher(email).matches()) {
            String[] arr = email.split("@");
            String a = arr[1];
            if (a.contains("gmail")) return a.equals("gmail.com");
            return a.contains(".");
        }
        return false;
    }

}
